package us.skyguardian.driverns6;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.gurtam.wiatagkit.Message;
import com.gurtam.wiatagkit.MessageSender;
import com.gurtam.wiatagkit.MessageSenderListener;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HomeActivity extends AppCompatActivity {
    DatabaseHelper bitacoraDb;
    private AppBarConfiguration mAppBarConfiguration;
    private static String  nombreUnidad, direccion;
    ProgressDialog progressDialog;
    private final static String CHANNEL_ID = "NOTIFICACIÓN";
    private final static int NOTIFICACION_ID = 0;
    private BottomNavigationView bottomNavigationView;
    Boolean banderaOnDuty, banderaConducir;
    private TextView origen, destino;
    AlertDialog alert = null;
    private TextView conductorVista;
    /****************************** DECLARACION DE BOTONES PARA FUNCIONES *************************************************/
    private ImageButton btnComida;
    private ImageButton btnDiesel;
    private ImageButton btnWC;
    private ImageButton btnDescanso;
    private ImageButton btnReparacionMecanica;
    private ImageButton btnTrafico;
    private ImageButton btnReten;
    private ImageButton btnClima;
    private ImageButton btnSos;
    private ImageButton btnCarga;
    private ImageButton btnDescarga;
    private ImageButton btnContinuar;
    private ImageButton btnPausa;
    private ImageButton btnMatt;
    private ImageButton btnDuty;
    private Chronometer chrChronometer;
    private Chronometer dhrChronometer;
    private TextView tituloNoti;
    private TextView chrtmp;
    private TextView dhrtmp;
    private TextView mTextField;
    private TextView acumuladoFecha;
    private SpannableString strNombre;
    private SpannableString strTelefono;
    private SpannableString strLlave;
    private SpannableString strTelematics;
    private SpannableString strId;
    private int idTelematics;
    private int idUsuario;
    private int validaAuxiliar;
    CountDownTimer countDownTimer;
    Handler handler = new Handler();
    Handler handler1 = new Handler();
    int delay  = 300000;
    int delayTimer = 50400000;
    int driveTime = 0;
    int restante = 0;
    Runnable runnable;
    Date date;
    Date date1;
    int BanderaTiempo = 0;
    boolean tipoNotificacion = false, conduccionTerminada = false;
    long descansoTotal;
    String idSync, acumuladoD, acumuladoC, conducir, onDuty, keySend, phoneSend, telematics, cuenta, userId;
    /*******************************************************************************/

    //private AdapterMensajes adapterMensajes;
    private String android_id = "", key, tel, receiveParam;

    private String auxQuery = "";
    private static String auxQueryID = "";

    private static final int LOCATION_COARSE = 100;
    private static final int LOCATION_FINE = 101;
    private static final int READ_PHONE_STATE = 107;

    double LongData, LatData;
    //Variables para el permiso de localización;
    private LocationManager locationManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FusedLocationProviderClient fusedLocationClient;
    private final static int ALL_PERMISSIONS_RESULT = 101;
    LocationTrack locationTrack;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();

    String[] linkLocation = {""};
    String[] MsgTexto = {" Realizó parada por comida ",                                  //1
            " Realizó parada por WC ",                                                   //2
            " Realizó parada para carga de combustible ",                                //3
            " Realizó parada para descansar ",                                           //4
            " Realizó parada por mal clima ",                                            //5
            " Realizó parada por una avería en la unidad ",                              //6
            " Realizó parada por reten ",                                                //7
            " Me encuentro en tráfico carretero ",                                       //8
            " SOS enviado, atenta reacción, favor de marcar o actuar de manera veloz ",  //9
            " En recepción de mercancia ",                                               //10
            " En entrega de mercancia ",                                                 //11
            " Conduciendo ",                                                             //12
            " Fuera de servicio ",                                                       //13
            " Realizó parada por una pausa ",                                            //14
            " Realizó parada por actividad auxiliar ",                                   //15
            " Continuando con el viaje ",                                                //16
            " En servicio ",                                                             //17
            " Sin conducir "                                                             //18
    };

    String host = "193.193.165.165"; //host y puerto de wialon
    int port = 20963;

    private String recibeUnidad = "", recibeTel = "", recibeVal = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        bitacoraDb = new DatabaseHelper(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_home:
                        Intent intentD = new Intent(HomeActivity.this, DatosUsuario.class);
                        intentD.putExtra("cuenta", cuenta);
                        intentD.putExtra("userId", userId);
                        startActivity(intentD);
                        break;
                    case R.id.nav_gallery:
                        Intent intentN = new Intent(HomeActivity.this, Normas.class);
                        startActivity(intentN);
                        break;
                    case R.id.nav_slideshow:
                        Intent intentR = new Intent(HomeActivity.this, ReporteBitacora.class);
                        intentR.putExtra("cuenta", cuenta);
                        intentR.putExtra("userId", userId);
                        startActivity(intentR);
                        break;
                    case R.id.nav_hist:
                        Intent intentH = new Intent(HomeActivity.this, Historial.class);
                        intentH.putExtra("cuenta", cuenta);
                        intentH.putExtra("userId", userId);
                        startActivity(intentH);
                        break;
                    case R.id.nav_close:
                        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                        Toast.makeText(getApplicationContext(), "Cerrando sesión...", Toast.LENGTH_LONG).show();
                        startActivity(intent);
                        break;
                }
                return false;
            }
        });


        tel = getIntent().getStringExtra("phone");
        key = getIntent().getStringExtra("llave");
        receiveParam = getIntent().getStringExtra("param");
        telematics = getIntent().getStringExtra("telematics");
        cuenta = getIntent().getStringExtra("cuenta");
        userId = getIntent().getStringExtra("userId");

        Cursor band = bitacoraDb.banderaConducirOnDuty();
        if (band.getCount() == 0) {
            System.out.println("Sin datos");
            banderaConducir = true;
            banderaOnDuty = true;
        }
        while (band.moveToNext()) {
            String bc = band.getString(0);
            String bo = band.getString(1);

            boolean booleandCond = Boolean.parseBoolean(bc);
            boolean booleanOnDuty = Boolean.parseBoolean(bo);

            banderaConducir = booleandCond;
            banderaOnDuty = booleanOnDuty;

            System.out.println("datos acumulados: " + banderaConducir + ", " + banderaOnDuty);
        }

        Cursor aux = bitacoraDb.validaAux();
        if (aux.getCount() == 0) {
            System.out.println("Sin datos de auxiliar");
        }
        while (aux.moveToNext()) {
            String bc = aux.getString(0);
            validaAuxiliar = Integer.parseInt(bc);
            System.out.println("datos auxiliares: " + bc);
            System.out.println("parse auxiliares: " + validaAuxiliar);
        }

        if(validaAuxiliar == 1){
            tipoNotificacion = true;
        }else{
            System.out.println("la notificación no es de tipo auxiliar");
        }

        handler.postDelayed(runnable = new Runnable() {
            public void run() {
                sincronizacion();
            }
        }, delay);


                String unitId = "";
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE))) {
                //mostrarExplicacionStatePhone();

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE);
            }
        } else {
            //El permiso ya fue activado
        }

        if (telephonyManager.getDeviceId() != null) {
            unitId = telephonyManager.getDeviceId();
            System.out.println("IMEI: " + unitId);
        }

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        //Valida si gps no esta encendido
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            AlertNoGps();
        }

        android_id = Settings.Secure.getString(HomeActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);

        String password = "C0ntr0lCCTSky19";
        MessageSender.initWithHost(host, port, key, password);


        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        /******************************* OBTENCIÓN DE ID's DE LA VISTA********************************************************/
        btnComida = findViewById(R.id.xml_imgBtnFood);
        btnWC = findViewById(R.id.xml_imgBtnWC);
        btnDiesel = findViewById(R.id.xml_imgBtnDiesel);
        btnDescanso = findViewById(R.id.xml_imgBtnDescanso);
        btnClima = findViewById(R.id.xml_imgBtnLlanta);
        btnReparacionMecanica = findViewById(R.id.xml_imgBtnReparacionMecanica);
        btnReten = findViewById(R.id.xml_imgBtnReten);
        btnTrafico = findViewById(R.id.xml_imgBtnTrafico);
        btnSos = findViewById(R.id.xml_btnImgSos);
        btnCarga = findViewById(R.id.xml_imgBtnCarga);
        btnDescarga = findViewById(R.id.xml_imgBtnDescarga);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_chat);
        btnContinuar = findViewById(R.id.xml_btnContinuar);
        btnPausa = findViewById(R.id.xml_imgBtnPausa);
        btnMatt = findViewById(R.id.xml_imgBtnMatt);
        btnDuty = findViewById(R.id.xml_btnOnDuty);
        conductorVista = findViewById(R.id.conductor);
        chrChronometer = (Chronometer) findViewById(R.id.simpleChronometer);
        dhrChronometer = (Chronometer) findViewById(R.id.simpleChronometer1);
        bottomNavigationView.setSelectedItemId(R.id.home_bottom);
        tituloNoti = findViewById(R.id.tituloNotificacion);
        chrtmp = findViewById(R.id.chrtmp);
        dhrtmp = findViewById(R.id.dhrtmp);
        mTextField = findViewById(R.id.timerField);
        acumuladoFecha = findViewById(R.id.fecha);
        /***************************************************************************************/

        bottomNavigationView.setSelectedItemId(R.id.home_bottom);

        /***********************Parametros que se obtienen de json al iniciar sesión************/
        try {
            JSONObject jo = new JSONObject(receiveParam);
            strNombre = new SpannableString(jo.getString("nombre"));
            conductorVista.setText(strNombre);
            strTelefono = new SpannableString(jo.getString("telefono"));
            strLlave = new SpannableString(jo.getString("llave"));
            strTelematics = new SpannableString(jo.getString("rid_telematics"));
            strId = new SpannableString(jo.getString("id"));

            System.out.println("nombre: " + strNombre + ", Telefono: " + strTelefono + ", LLave: " + strLlave + ", idTelematics: " + strTelematics + ", ID: " + strId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        cuenta = String.valueOf(strTelematics);
        userId = String.valueOf(strId);
        System.out.println("casteando cuenta: " + cuenta);

        telematics = String.valueOf(strTelematics);
        String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        restanteManejo();
        countDownTimer.cancel();

        acumuladoFecha.setText("Acumulado (" +  currentDate + ")");

        /***************************************************************************************/

        Cursor resA = bitacoraDb.Acumulado();
        if (resA.getCount() == 0) {
            System.out.println("Sin datos");
            acumuladoC = "00:00:00";
            acumuladoD = "00:00:00";
            chrtmp.setText(acumuladoC);
            dhrtmp.setText(acumuladoD);
        }
        while (resA.moveToNext()) {
            acumuladoD = resA.getString(0);
            acumuladoC = resA.getString(1);

            System.out.println("datos acumulados: " + acumuladoD + ", " + acumuladoC);

            chrtmp.setText(acumuladoC);
            dhrtmp.setText(acumuladoD);
        }

        idTelematics = Integer.parseInt(String.valueOf(strTelematics));

        idUsuario = Integer.parseInt(String.valueOf(strId));

        System.out.println("parse id telematics: " + idTelematics);
        System.out.println("parse de id de usuario: " + idUsuario);
        if (banderaOnDuty == true) {
            btnContinuar.setImageResource(R.drawable.conducir_off);
            btnComida.setEnabled(false);
            btnWC.setEnabled(false);
            btnDiesel.setEnabled(false);
            btnDescanso.setEnabled(false);
            btnClima.setEnabled(false);
            btnReparacionMecanica.setEnabled(false);
            btnReten.setEnabled(false);
            btnTrafico.setEnabled(false);
            btnCarga.setEnabled(false);
            btnDescarga.setEnabled(false);
            btnPausa.setEnabled(false);
            btnMatt.setEnabled(false);
            btnDuty.setImageResource(R.drawable.offduty);
            btnComida.setImageResource(R.drawable.comida_off);
            btnWC.setImageResource(R.drawable.banos_off);
            btnDiesel.setImageResource(R.drawable.combustible_off);
            btnDescanso.setImageResource(R.drawable.descanso_off);
            btnClima.setImageResource(R.drawable.clima_off);
            btnReparacionMecanica.setImageResource(R.drawable.averia_off);
            btnReten.setImageResource(R.drawable.revision_off);
            btnTrafico.setImageResource(R.drawable.trafico_off);
            btnCarga.setImageResource(R.drawable.cargando_off);
            btnDescarga.setImageResource(R.drawable.entrega_off);
            btnPausa.setImageResource(R.drawable.pausa_off);
            btnMatt.setImageResource(R.drawable.matt_off);
        } else {
            btnDuty.setImageResource(R.drawable.onduty);
            btnComida.setEnabled(true);
            btnWC.setEnabled(true);
            btnDiesel.setEnabled(true);
            btnDescanso.setEnabled(true);
            btnClima.setEnabled(true);
            btnReparacionMecanica.setEnabled(true);
            btnReten.setEnabled(true);
            btnTrafico.setEnabled(true);
            btnCarga.setEnabled(true);
            btnDescarga.setEnabled(true);
            btnPausa.setEnabled(true);
            btnMatt.setEnabled(true);
            btnComida.setImageResource(R.drawable.comida);
            btnWC.setImageResource(R.drawable.banos);
            btnDiesel.setImageResource(R.drawable.combustible);
            btnDescanso.setImageResource(R.drawable.descanso);
            btnClima.setImageResource(R.drawable.clima);
            btnReparacionMecanica.setImageResource(R.drawable.averia);
            btnReten.setImageResource(R.drawable.revision_design);
            btnTrafico.setImageResource(R.drawable.trafico_new);
            btnCarga.setImageResource(R.drawable.cargando_design);
            btnDescarga.setImageResource(R.drawable.entrega_design);
            btnPausa.setImageResource(R.drawable.pausa);
            btnMatt.setImageResource(R.drawable.matt);
        }

        if (banderaConducir == true) {
            btnContinuar.setImageResource(R.drawable.conducir_off);
            restanteManejo();
            countDownTimer.cancel();
        } else {
            btnContinuar.setImageResource(R.drawable.conduciendo_notidriver);
            tipoNotificacion = true;
            restanteManejo();
        }

        /******************************* CAMBIO DE ACTIVIDADES CON EL BOTTOM NAVIGATION VIEW ********************************************************/

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.bitacora_bottom:
                        Intent intent = new Intent(HomeActivity.this, Bitacora.class);
                            intent.putExtra("valida", "1");
                            intent.putExtra("param", receiveParam);
                            intent.putExtra("llave", key);
                            intent.putExtra("phone", tel);
                            intent.putExtra("telmatics", telematics);
                            intent.putExtra("cuenta", cuenta);
                            intent.putExtra("userId", userId);
                        receiveParam = getIntent().getStringExtra("param");
                        startActivity(intent);
                        break;
                    case R.id.home_bottom:
                        break;
                    case R.id.curso_bottom:
                        Intent intent3 = new Intent(HomeActivity.this, actividades.class);
                            intent3.putExtra("valida", "1");
                            intent3.putExtra("llave", key);
                            intent3.putExtra("phone", tel);
                            intent3.putExtra("paramH", receiveParam);
                            intent3.putExtra("telmatics", telematics);
                            intent3.putExtra("cuenta", cuenta);
                            intent3.putExtra("userId", userId);
                        startActivity(intent3);
                        break;
                    case R.id.agenda_bottom:
                        Intent intent2 = new Intent(HomeActivity.this, AgendaActivity.class);
                            intent2.putExtra("valida", "1");
                            intent2.putExtra("llave", key);
                            intent2.putExtra("unit", recibeUnidad);
                            intent2.putExtra("phone", tel);
                            intent2.putExtra("paramH", receiveParam);
                            intent2.putExtra("telmatics", telematics);
                            intent2.putExtra("cuenta", cuenta);
                            intent2.putExtra("userId", userId);
                        startActivity(intent2);
                        break;
                }
                return true;
            }

        });

        /********************************* AGREGA FUNCION DE LOS BOTONES DE ACTIVIDADES SECUNDARIOS *************************************************************/
        btnDuty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                if (banderaOnDuty == false) {
                    banderaConducir = true;
                    btnContinuar.setImageResource(R.drawable.conducir_off);
                    countDownTimer.cancel();
                    banderaOnDuty = true;
                    btnComida.setEnabled(false);
                    btnWC.setEnabled(false);
                    btnDiesel.setEnabled(false);
                    btnDescanso.setEnabled(false);
                    btnClima.setEnabled(false);
                    btnReparacionMecanica.setEnabled(false);
                    btnReten.setEnabled(false);
                    btnTrafico.setEnabled(false);
                    btnCarga.setEnabled(false);
                    btnDescarga.setEnabled(false);
                    btnPausa.setEnabled(false);
                    btnMatt.setEnabled(false);
                    btnDuty.setImageResource(R.drawable.offduty);
                    btnComida.setImageResource(R.drawable.comida_off);
                    btnWC.setImageResource(R.drawable.banos_off);
                    btnDiesel.setImageResource(R.drawable.combustible_off);
                    btnDescanso.setImageResource(R.drawable.descanso_off);
                    btnClima.setImageResource(R.drawable.clima_off);
                    btnReparacionMecanica.setImageResource(R.drawable.averia_off);
                    btnReten.setImageResource(R.drawable.revision_off);
                    btnTrafico.setImageResource(R.drawable.trafico_off);
                    btnCarga.setImageResource(R.drawable.cargando_off);
                    btnDescarga.setImageResource(R.drawable.entrega_off);
                    btnPausa.setImageResource(R.drawable.pausa_off);
                    btnMatt.setImageResource(R.drawable.matt_off);
                    int validaInternet = verificarSenal();
                    conducir = "" + banderaConducir;
                    onDuty = "" + banderaOnDuty;
                    if (validaInternet == 1) {
                        locationTrack = new LocationTrack(HomeActivity.this);
                        if (locationTrack.canGetLocation()) {
                            double longitude = locationTrack.getLongitude();
                            double latitude = locationTrack.getLatitude();
                            LongData = longitude;
                            LatData = latitude;
                        } else {
                            locationTrack.showSettingsAlert();
                        }

                        try {
                            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                            if (addresses.size() > 0) {
                                String address1 = addresses.get(0).getAddressLine(0);
                                direccion = address1;
                                System.out.println("dirección solo coordenadas: " + address1);
                            }
                        } catch (IOException e) {
                            Log.e("tag", e.getMessage());
                        }

                        boolean isInserted = bitacoraDb.insertData(6, 14, 4, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1, "00:00:00", "00:00:00", conducir, onDuty, 0);
                        if (isInserted = true) {
                            tituloNoti.setText("Fuera de Servicio");
                            tituloNoti.setTextColor(Color.parseColor("#c6c6c6"));
                            dhrChronometer.setTextColor(Color.parseColor("#c6c6c6"));
                            BanderaTiempo = 3;
                            //Toast.makeText(getApplicationContext(), "notificación fuera de servicio insertada correctamente.", Toast.LENGTH_LONG).show();
                            Tiempo();
                        } else {
                            System.out.println("notificación fuera de servicio no insertada correctamente.");
                        }
                        accederPermisoLocation(MsgTexto[12], 1);
                    } else {
                        if (validaInternet == 0) {
                            Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                            boolean isInserted = bitacoraDb.insertData(6, 14, 4, currentDate,
                                    currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0, "00:00:00", "00:00:00", conducir, onDuty, 0);
                            if (isInserted = true) {
                                tituloNoti.setText("Fuera de Servicio");
                                tituloNoti.setTextColor(Color.parseColor("#c6c6c6"));
                                dhrChronometer.setTextColor(Color.parseColor("#c6c6c6"));
                                BanderaTiempo = 3;
                                Tiempo();
                            } else {
                                System.out.println("notificación fuera de servicio no insertada correctamente.");
                            }
                        }
                    }
                    AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                    LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                    final View view2 = factory.inflate(R.layout.image_assistant, null);
                    final ImageView dialogImageView = (ImageView) view2.findViewById(R.id.iv);
                    dialogImageView.setImageResource(R.drawable.happy_assistant);
                    alertadd.setView(view2);
                    alertadd.setCancelable(false);
                    alertadd.setTitle("¡Fin de Jornada!");
                    alertadd.setMessage("Que descanses.");
                    alertadd.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dlg, int sumthin) {
                            finalizando();
                        }
                    });
                    alertadd.show();
                } else {
                    validaReinicio();
                    countDownTimer.cancel();
                    banderaOnDuty = false;
                    btnDuty.setImageResource(R.drawable.onduty);
                    btnComida.setEnabled(true);
                    btnWC.setEnabled(true);
                    btnDiesel.setEnabled(true);
                    btnDescanso.setEnabled(true);
                    btnClima.setEnabled(true);
                    btnReparacionMecanica.setEnabled(true);
                    btnReten.setEnabled(true);
                    btnTrafico.setEnabled(true);
                    btnCarga.setEnabled(true);
                    btnDescarga.setEnabled(true);
                    btnPausa.setEnabled(true);
                    btnMatt.setEnabled(true);
                    btnComida.setImageResource(R.drawable.comida);
                    btnWC.setImageResource(R.drawable.banos);
                    btnDiesel.setImageResource(R.drawable.combustible);
                    btnDescanso.setImageResource(R.drawable.descanso);
                    btnClima.setImageResource(R.drawable.clima);
                    btnReparacionMecanica.setImageResource(R.drawable.averia);
                    btnReten.setImageResource(R.drawable.revision_design);
                    btnTrafico.setImageResource(R.drawable.trafico_new);
                    btnCarga.setImageResource(R.drawable.cargando_design);
                    btnDescarga.setImageResource(R.drawable.entrega_design);
                    btnPausa.setImageResource(R.drawable.pausa);
                    btnMatt.setImageResource(R.drawable.matt);
                    conducir = "" + banderaConducir;
                    onDuty = "" + banderaOnDuty;
                    int validaInternet = verificarSenal();
                    if (validaInternet == 1) {
                        locationTrack = new LocationTrack(HomeActivity.this);
                        if (locationTrack.canGetLocation()) {
                            double longitude = locationTrack.getLongitude();
                            double latitude = locationTrack.getLatitude();
                            LongData = longitude;
                            LatData = latitude;
                        } else {
                            locationTrack.showSettingsAlert();
                        }
                        try {
                            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                            if (addresses.size() > 0) {
                                String address1 = addresses.get(0).getAddressLine(0);
                                direccion = address1;
                                System.out.println("dirección solo coordenadas: " + address1);
                            }
                        } catch (IOException e) {
                            Log.e("tag", e.getMessage());
                        }

                        boolean isInserted = bitacoraDb.insertData(6, 13, 4, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1, "00:00:00", "00:00:00", conducir, onDuty, 0);
                        if (isInserted = true) {
                            tituloNoti.setText("En Servicio");
                            tituloNoti.setTextColor(Color.parseColor("#c6c6c6"));
                            dhrChronometer.setTextColor(Color.parseColor("#c6c6c6"));
                            BanderaTiempo = 3;
                            Tiempo();
                        } else {
                            Toast.makeText(getApplicationContext(), "notificación en servicio no insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                        accederPermisoLocation(MsgTexto[16], 1);
                    } else {
                        if (validaInternet == 0) {
                            Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                            boolean isInserted = bitacoraDb.insertData(6, 13, 4, currentDate,
                                    currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0, "00:00:00", "00:00:00", conducir, onDuty, 0);
                            if (isInserted = true) {
                                tituloNoti.setText("En Servicio");
                                tituloNoti.setTextColor(Color.parseColor("#c6c6c6"));
                                dhrChronometer.setTextColor(Color.parseColor("#c6c6c6"));
                                BanderaTiempo = 3;
                                Toast.makeText(getApplicationContext(), "notificación en servicio insertada correctamente.", Toast.LENGTH_LONG).show();
                                Tiempo();
                            } else {
                                Toast.makeText(getApplicationContext(), "notificación en servicio no insertada correctamente.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                    AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                    LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                    final View view1 = factory.inflate(R.layout.image_assistant, null);
                    final ImageView dialogImageView = (ImageView) view1.findViewById(R.id.iv);
                    dialogImageView.setImageResource(R.drawable.happy_assistant);
                    alertadd.setView(view1);
                    alertadd.setCancelable(false);
                    alertadd.setTitle("¡Hola "+ strNombre + "!");
                    alertadd.setMessage("¡Has iniciado una nueva jornada, presiona el botón 'Conducir', cuando etés en camino. buen viaje!");
                    alertadd.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dlg, int sumthin) {
                        }
                    });
                    alertadd.show();
                }
            }
        });

        btnComida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                banderaConducir = true;
                btnContinuar.setImageResource(R.drawable.conducir_off);
                countDownTimer.cancel();
                Toast.makeText(HomeActivity.this, "Conductor no está conduciendo", Toast.LENGTH_LONG).show();

                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

                int validaInternet = verificarSenal();
                conducir = "" + banderaConducir;
                onDuty = "" + banderaOnDuty;
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    try {
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                        if (addresses.size() > 0) {
                            String address1 = addresses.get(0).getAddressLine(0);
                            direccion = address1;
                            System.out.println("dirección solo coordenadas: " + address1);
                        }
                    } catch (IOException e) {
                        Log.e("tag", e.getMessage());
                    }
                    boolean isInserted = bitacoraDb.insertData(3, 2, 1, currentDate,
                            currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1, "00:00:00", "00:00:00", conducir, onDuty, 0);
                    if (isInserted = true) {
                        tituloNoti.setText("Pausa");
                        tituloNoti.setTextColor(Color.parseColor("#f11d06"));
                        dhrChronometer.setTextColor(Color.parseColor("#f11d06"));
                        BanderaTiempo = 3;
                        Tiempo();
                    } else {

                    }
                    accederPermisoLocation(MsgTexto[0], 1);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                        boolean isInserted = bitacoraDb.insertData(3, 2, 1, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0, "00:00:00", "00:00:00", conducir, onDuty, 0);
                        if (isInserted = true) {
                            tituloNoti.setText("Pausa");
                            tituloNoti.setTextColor(Color.parseColor("#f11d06"));
                            chrChronometer.setTextColor(Color.parseColor("#f11d06"));
                            BanderaTiempo = 3;
                            Tiempo();
                        } else {
                            Toast.makeText(getApplicationContext(), "notificación comida no insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                final View view1 = factory.inflate(R.layout.image_assistant, null);
                final ImageView dialogImageView = (ImageView) view1.findViewById(R.id.iv);
                dialogImageView.setImageResource(R.drawable.happy_assistant);
                alertadd.setView(view1);
                alertadd.setTitle("¡"+ strNombre +"!");
                alertadd.setMessage("Estas en una pausa, no olvides informar cuando reanudes tu vaje o realices alguna otra operación");
                alertadd.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int sumthin) {
                    }
                });
                alertadd.show();
            }
        });

        btnWC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                banderaConducir = true;
                btnContinuar.setImageResource(R.drawable.conducir_off);
                countDownTimer.cancel();
                Toast.makeText(HomeActivity.this, "Conductor no está conduciendo", Toast.LENGTH_LONG).show();
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                int validaInternet = verificarSenal();
                conducir = "" + banderaConducir;
                onDuty = "" + banderaOnDuty;
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    try {
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                        if (addresses.size() > 0) {
                            String address1 = addresses.get(0).getAddressLine(0);
                            direccion = address1;
                            System.out.println("dirección solo coordenadas: " + address1);
                        }
                    } catch (IOException e) {
                        Log.e("tag", e.getMessage());
                    }
                    boolean isInserted = bitacoraDb.insertData(3, 3, 1, currentDate,
                            currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1, "00:00:00", "00:00:00", conducir, onDuty, 0);
                    if (isInserted = true) {
                        tituloNoti.setText("Pausa");
                        tituloNoti.setTextColor(Color.parseColor("#f11d06"));
                        dhrChronometer.setTextColor(Color.parseColor("#f11d06"));
                        BanderaTiempo = 3;
                        Tiempo();
                    } else {
                        Toast.makeText(getApplicationContext(), "notificación wc no insertada correctamente.", Toast.LENGTH_LONG).show();
                    }
                    accederPermisoLocation(MsgTexto[1], 2);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                        boolean isInserted = bitacoraDb.insertData(3, 3, 1, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0, "00:00:00", "00:00:00", conducir, onDuty, 0);
                        if (isInserted = true) {
                            tituloNoti.setText("Pausa");
                            tituloNoti.setTextColor(Color.parseColor("#f11d06"));
                            chrChronometer.setTextColor(Color.parseColor("#f11d06"));
                            BanderaTiempo = 3;
                            Toast.makeText(getApplicationContext(), "notificación wc insertada correctamente.", Toast.LENGTH_LONG).show();
                            Tiempo();
                        } else {
                            Toast.makeText(getApplicationContext(), "notificación wc no insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                final View view1 = factory.inflate(R.layout.image_assistant, null);
                final ImageView dialogImageView = (ImageView) view1.findViewById(R.id.iv);
                dialogImageView.setImageResource(R.drawable.happy_assistant);
                alertadd.setView(view1);
                alertadd.setTitle("¡"+ strNombre +"!");
                alertadd.setMessage("Estas en una pausa, no olvides informar cuando reanudes tu vaje o realices alguna otra operación");
                alertadd.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int sumthin) {
                    }
                });
                alertadd.show();
            }
        });

        btnDescanso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                banderaConducir = true;
                btnContinuar.setImageResource(R.drawable.conducir_off);
                countDownTimer.cancel();
                Toast.makeText(HomeActivity.this, "Conductor no está conduciendo", Toast.LENGTH_LONG).show();
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                int validaInternet = verificarSenal();
                conducir = "" + banderaConducir;
                onDuty = "" + banderaOnDuty;
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    try {
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                        if (addresses.size() > 0) {
                            String address1 = addresses.get(0).getAddressLine(0);
                            direccion = address1;
                            System.out.println("dirección solo coordenadas: " + address1);
                        }
                    } catch (IOException e) {
                        Log.e("tag", e.getMessage());
                    }
                    boolean isInserted = bitacoraDb.insertData(3, 4, 1, currentDate,
                            currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1,"00:00:00", "00:00:00", conducir, onDuty, 0);
                    if(isInserted = true){
                        tituloNoti.setText("Descanso");
                        tituloNoti.setTextColor(Color.parseColor("#f11d06"));
                        dhrChronometer.setTextColor(Color.parseColor("#f11d06"));
                        BanderaTiempo = 3;
                        Tiempo();
                    }else{
                        Toast.makeText(getApplicationContext(), "notificación descanso no insertada correctamente.", Toast.LENGTH_LONG).show();
                    }
                    accederPermisoLocation(MsgTexto[3], 4);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                        boolean isInserted = bitacoraDb.insertData(3, 4, 1, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0,"00:00:00", "00:00:00", conducir, onDuty, 0);
                        if(isInserted = true){
                            tituloNoti.setText("Descanso");
                            tituloNoti.setTextColor(Color.parseColor("#f11d06"));
                            chrChronometer.setTextColor(Color.parseColor("#f11d06"));
                            BanderaTiempo = 3;
                            Tiempo();
                        }else{
                            Toast.makeText(getApplicationContext(), "notificación descanso no insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                final View view1 = factory.inflate(R.layout.image_assistant, null);
                final ImageView dialogImageView = (ImageView) view1.findViewById(R.id.iv);
                dialogImageView.setImageResource(R.drawable.happy_assistant);
                alertadd.setView(view1);
                alertadd.setTitle("¡"+ strNombre +"!");
                alertadd.setMessage("Estas en una pausa, no olvides informar cuando reanudes tu vaje o realices alguna otra operación");
                alertadd.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int sumthin) {
                    }
                });
                alertadd.show();
            }
        });

        btnPausa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                banderaConducir = true;
                btnContinuar.setImageResource(R.drawable.conducir_off);
                countDownTimer.cancel();
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                int validaInternet = verificarSenal();
                conducir = "" + banderaConducir;
                onDuty = "" + banderaOnDuty;
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    try {
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                        if (addresses.size() > 0) {
                            String address1 = addresses.get(0).getAddressLine(0);
                            direccion = address1;
                            System.out.println("dirección solo coordenadas: " + address1);
                        }
                    } catch (IOException e) {
                        Log.e("tag", e.getMessage());
                    }
                    boolean isInserted = bitacoraDb.insertData(3, 1, 1, currentDate,
                            currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1,"00:00:00", "00:00:00", conducir, onDuty, 0);
                    if(isInserted = true){
                        tituloNoti.setText("Pausa");
                        tituloNoti.setTextColor(Color.parseColor("#f11d06"));
                        dhrChronometer.setTextColor(Color.parseColor("#f11d06"));
                        BanderaTiempo = 3;
                        Tiempo();
                    }else{
                        Toast.makeText(getApplicationContext(), "notificación pausa insertada correctamente.", Toast.LENGTH_LONG).show();
                    }
                    accederPermisoLocation(MsgTexto[13], 1);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                        boolean isInserted = bitacoraDb.insertData(3, 1, 1, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0,"00:00:00", "00:00:00", conducir, onDuty, 0);
                        if(isInserted = true){
                            tituloNoti.setText("Pausa");
                            tituloNoti.setTextColor(Color.parseColor("#f11d06"));
                            dhrChronometer.setTextColor(Color.parseColor("#f11d06"));
                            BanderaTiempo = 3;
                            Tiempo();
                        }else{
                            Toast.makeText(getApplicationContext(), "notificación pausa insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                final View view1 = factory.inflate(R.layout.image_assistant, null);
                final ImageView dialogImageView = (ImageView) view1.findViewById(R.id.iv);
                dialogImageView.setImageResource(R.drawable.happy_assistant);
                alertadd.setView(view1);
                alertadd.setTitle("¡"+ strNombre +"!");
                alertadd.setMessage("Estas en una pausa, no olvides informar cuando reanudes tu vaje o realices alguna otra operación");
                alertadd.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int sumthin) {
                    }
                });
                alertadd.show();
            }
        });

        btnClima.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countDownTimer.cancel();
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                int validaInternet = verificarSenal();
                conducir = "" + banderaConducir;
                onDuty = "" + banderaOnDuty;
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    try {
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                        if (addresses.size() > 0) {
                            String address1 = addresses.get(0).getAddressLine(0);
                            direccion = address1;
                            System.out.println("dirección solo coordenadas: " + address1);
                        }
                    } catch (IOException e) {
                        Log.e("tag", e.getMessage());
                    }
                    boolean isInserted = bitacoraDb.insertData(5, 11, 3, currentDate,
                            currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1,dhrtmp.getText().toString(), chrtmp.getText().toString(), conducir, onDuty, 0);
                    if(isInserted = true){
                        tituloNoti.setText("Casos de Excepción");
                        tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                        dhrChronometer.setVisibility(View.INVISIBLE);
                        chrChronometer.setVisibility(View.VISIBLE);
                        chrChronometer.setBase(SystemClock.elapsedRealtime());
                        chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                        BanderaTiempo = 2;
                    }else{
                        Toast.makeText(getApplicationContext(), "notificación clima insertada correctamente.", Toast.LENGTH_LONG).show();
                    }
                    accederPermisoLocation(MsgTexto[4], 5);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                        boolean isInserted = bitacoraDb.insertData(5, 11, 3, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0,"00:00:00", "00:00:00", conducir, onDuty, 0);
                        if(isInserted = true){
                            tituloNoti.setText("Casos de Excepción");
                            tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                            dhrChronometer.setVisibility(View.INVISIBLE);
                            chrChronometer.setVisibility(View.VISIBLE);
                            chrChronometer.setBase(SystemClock.elapsedRealtime());
                            chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                            BanderaTiempo = 2;
                        }else{
                            Toast.makeText(getApplicationContext(), "notificación clima no insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                final View view1 = factory.inflate(R.layout.image_assistant, null);
                final ImageView dialogImageView = (ImageView) view1.findViewById(R.id.iv);
                dialogImageView.setImageResource(R.drawable.sad_assistant);
                alertadd.setView(view1);
                alertadd.setTitle("¡Qué mal!");
                alertadd.setMessage("Cuando las cosas se arreglen, danos aviso de que prosigues tu cmamino, presionando el botón 'Conducir' en el ícono del volante.");
                alertadd.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int sumthin) {
                    }
                });
                alertadd.show();
            }
        });

        btnReparacionMecanica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countDownTimer.cancel();
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                int validaInternet = verificarSenal();
                conducir = "" + banderaConducir;
                onDuty = "" + banderaOnDuty;
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    try {
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                        if (addresses.size() > 0) {
                            String address1 = addresses.get(0).getAddressLine(0);
                            direccion = address1;
                            //resultado.append(address.getLocality()).append("\n");
                            //resultado.append(address.getCountryName());
                            System.out.println("dirección solo coordenadas: " + address1);
                        }
                    } catch (IOException e) {
                        Log.e("tag", e.getMessage());
                    }
                    boolean isInserted = bitacoraDb.insertData(5, 9, 3, currentDate,
                            currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1,dhrtmp.getText().toString(), chrtmp.getText().toString(), conducir, onDuty, 0);
                    if(isInserted = true){
                        tituloNoti.setText("Casos de Excepción");
                        tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                        dhrChronometer.setVisibility(View.INVISIBLE);
                        chrChronometer.setVisibility(View.VISIBLE);
                        chrChronometer.setBase(SystemClock.elapsedRealtime());
                        chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                        BanderaTiempo = 2;
                    }else{
                        Toast.makeText(getApplicationContext(), "notificación avería no insertada correctamente.", Toast.LENGTH_LONG).show();
                    }
                    accederPermisoLocation(MsgTexto[5], 6);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                        boolean isInserted = bitacoraDb.insertData(5, 9, 3, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0,"00:00:00", "00:00:00", conducir, onDuty, 0);
                        if(isInserted = true){
                            tituloNoti.setText("Casos de Excepción");
                            tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                            dhrChronometer.setVisibility(View.INVISIBLE);
                            chrChronometer.setVisibility(View.VISIBLE);
                            chrChronometer.setBase(SystemClock.elapsedRealtime());
                            chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                            BanderaTiempo = 2;
                        }else{
                            Toast.makeText(getApplicationContext(), "notificación avería no insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                final View view1 = factory.inflate(R.layout.image_assistant, null);
                final ImageView dialogImageView = (ImageView) view1.findViewById(R.id.iv);
                dialogImageView.setImageResource(R.drawable.sad_assistant);
                alertadd.setView(view1);
                alertadd.setTitle("¡Qué mal!");
                alertadd.setMessage("Cuando las cosas se arreglen, danos aviso de que prosigues tu cmamino, presionando el botón 'Conducir' en el ícono del volante.");
                alertadd.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int sumthin) {
                    }
                });
                alertadd.show();
            }
        });

        btnReten.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countDownTimer.cancel();
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                int validaInternet = verificarSenal();
                conducir = "" + banderaConducir;
                onDuty = "" + banderaOnDuty;
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    try {
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                        if (addresses.size() > 0) {
                            String address1 = addresses.get(0).getAddressLine(0);
                            direccion = address1;
                            //resultado.append(address.getLocality()).append("\n");
                            //resultado.append(address.getCountryName());
                            System.out.println("dirección solo coordenadas: " + address1);
                        }
                    } catch (IOException e) {
                        Log.e("tag", e.getMessage());
                    }
                    boolean isInserted = bitacoraDb.insertData(5, 10, 3, currentDate,
                            currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1,dhrtmp.getText().toString(), chrtmp.getText().toString(), conducir, onDuty, 0);
                    if(isInserted = true){
                        tituloNoti.setText("Casos de Excepción");
                        tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                        dhrChronometer.setVisibility(View.INVISIBLE);
                        chrChronometer.setVisibility(View.VISIBLE);
                        chrChronometer.setBase(SystemClock.elapsedRealtime());
                        chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                        BanderaTiempo = 2;
                    }else{
                        Toast.makeText(getApplicationContext(), "notificación revisión no insertada correctamente.", Toast.LENGTH_LONG).show();
                    }
                    accederPermisoLocation(MsgTexto[6], 7);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                        boolean isInserted = bitacoraDb.insertData(5, 10, 3, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0,dhrtmp.getText().toString(), chrtmp.getText().toString(), conducir, onDuty, 0);
                        if(isInserted = true){
                            tituloNoti.setText("Casos de Excepción");
                            tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                            dhrChronometer.setVisibility(View.INVISIBLE);
                            chrChronometer.setVisibility(View.VISIBLE);
                            chrChronometer.setBase(SystemClock.elapsedRealtime());
                            chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                            BanderaTiempo = 2;
                        }else{
                            Toast.makeText(getApplicationContext(), "notificación revisión no insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                final View view1 = factory.inflate(R.layout.image_assistant, null);
                final ImageView dialogImageView = (ImageView) view1.findViewById(R.id.iv);
                dialogImageView.setImageResource(R.drawable.sad_assistant);
                alertadd.setView(view1);
                alertadd.setTitle("¡Qué mal!");
                alertadd.setMessage("Cuando las cosas se arreglen, danos aviso de que prosigues tu cmamino, presionando el botón 'Conducir' en el ícono del volante.");
                alertadd.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int sumthin) {
                    }
                });
                alertadd.show();
            }
        });

        btnTrafico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countDownTimer.cancel();
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                int validaInternet = verificarSenal();
                conducir = "" + banderaConducir;
                onDuty = "" + banderaOnDuty;
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    try {
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                        if (addresses.size() > 0) {
                            String address1 = addresses.get(0).getAddressLine(0);
                            direccion = address1;
                            //resultado.append(address.getLocality()).append("\n");
                            //resultado.append(address.getCountryName());
                            System.out.println("dirección solo coordenadas: " + address1);
                        }
                    } catch (IOException e) {
                        Log.e("tag", e.getMessage());
                    }
                    boolean isInserted = bitacoraDb.insertData(5, 12, 3, currentDate,
                            currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1,dhrtmp.getText().toString(), chrtmp.getText().toString(), conducir, onDuty, 0);
                    if(isInserted = true){
                        tituloNoti.setText("Casos de Excepción");
                        tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                        dhrChronometer.setVisibility(View.INVISIBLE);
                        chrChronometer.setVisibility(View.VISIBLE);
                        chrChronometer.setBase(SystemClock.elapsedRealtime());
                        chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                        BanderaTiempo = 2;
                    }else{
                        Toast.makeText(getApplicationContext(), "notificación trafico no insertada correctamente.", Toast.LENGTH_LONG).show();
                    }
                    accederPermisoLocation(MsgTexto[7], 8);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                        boolean isInserted = bitacoraDb.insertData(5, 12, 3, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0,"00:00:00", "00:00:00", conducir, onDuty, 0);
                        if(isInserted = true){
                            tituloNoti.setText("Casos de Excepción");
                            tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                            dhrChronometer.setVisibility(View.INVISIBLE);
                            chrChronometer.setVisibility(View.VISIBLE);
                            chrChronometer.setBase(SystemClock.elapsedRealtime());
                            chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                            BanderaTiempo = 2;
                        }else{
                            Toast.makeText(getApplicationContext(), "notificación trafico no insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                final View view1 = factory.inflate(R.layout.image_assistant, null);
                final ImageView dialogImageView = (ImageView) view1.findViewById(R.id.iv);
                dialogImageView.setImageResource(R.drawable.sad_assistant);
                alertadd.setView(view1);
                alertadd.setTitle("¡Qué mal!");
                alertadd.setMessage("Cuando las cosas se arreglen, danos aviso de que prosigues tu cmamino, presionando el botón 'Conducir' en el ícono del volante.");
                alertadd.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int sumthin) {
                    }
                });
                alertadd.show();
            }
        });

        btnSos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                int validaInternet = verificarSenal();
                conducir = "" + banderaConducir;
                onDuty = "" + banderaOnDuty;
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    try {
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                        if (addresses.size() > 0) {
                            String address1 = addresses.get(0).getAddressLine(0);
                            direccion = address1;
                            System.out.println("dirección solo coordenadas: " + address1);
                        }
                    } catch (IOException e) {
                        Log.e("tag", e.getMessage());
                    }
                    boolean isInserted = bitacoraDb.insertData(7, 16, 4, currentDate,
                            currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1,"00:00:00", "00:00:00", conducir, onDuty, 0);
                    if(isInserted = true){
                    }else{
                        Toast.makeText(getApplicationContext(), "notificación sos no insertada correctamente.", Toast.LENGTH_LONG).show();
                    }
                    accederPermisoLocation(MsgTexto[8], 9);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                        boolean isInserted = bitacoraDb.insertData(7, 16, 4, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0,"00:00:00", "00:00:00", conducir, onDuty, 0);
                        if(isInserted = true){
                        }else{
                            Toast.makeText(getApplicationContext(), "notificación sos no insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });

        btnDiesel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restanteManejo();
                banderaConducir = true;
                btnContinuar.setImageResource(R.drawable.conducir_off);
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                int validaInternet = verificarSenal();
                conducir = "" + banderaConducir;
                onDuty = "" + banderaOnDuty;
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    try {
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                        if (addresses.size() > 0) {
                            String address1 = addresses.get(0).getAddressLine(0);
                            direccion = address1;
                            System.out.println("dirección solo coordenadas: " + address1);
                        }
                    } catch (IOException e) {
                        Log.e("tag", e.getMessage());
                    }
                    boolean isInserted = bitacoraDb.insertData(2, 7, 2, currentDate,
                            currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1,"00:00:00", "00:00:00", conducir, onDuty, 1);
                    if(isInserted = true){
                        tituloNoti.setText("Actividades Auxiliares");
                        tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                        chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                        BanderaTiempo = 1;
                        Tiempo();
                    }else{
                        Toast.makeText(getApplicationContext(), "notificación gasolina no insertada correctamente.", Toast.LENGTH_LONG).show();
                    }
                    accederPermisoLocation(MsgTexto[2], 3);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                        boolean isInserted = bitacoraDb.insertData(2, 7, 2, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0,"00:00:00", "00:00:00",conducir, onDuty, 1);
                        if(isInserted = true){
                            tituloNoti.setText("Actividades Auxiliares");
                            tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                            chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                            BanderaTiempo = 1;
                            //Toast.makeText(getApplicationContext(), "notificación gasolina insertada correctamente.", Toast.LENGTH_LONG).show();
                            Tiempo();
                        }else{
                            Toast.makeText(getApplicationContext(), "notificación gasolina no insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                final View view1 = factory.inflate(R.layout.image_assistant, null);
                final ImageView dialogImageView = (ImageView) view1.findViewById(R.id.iv);
                dialogImageView.setImageResource(R.drawable.happy_assistant);
                alertadd.setView(view1);
                alertadd.setTitle("¡Buen trabajo "+ strNombre + "!");
                alertadd.setMessage("Las cargas y entregas a tiempo hablan bien de todos nosotros! Cuando concluyas presiona continuar en el icono del volante.");
                alertadd.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int sumthin) {
                        Intent browseintent=new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://telematics.skyguardian.mx/login.html?client_id=Fuelapp&access_type=-1&lang=es&activation_time=0&duration=0&flags=0x1&redirect_uri=http://192.169.213.91:9001/apps/pagogas/procesologin.php"));
                        startActivity(browseintent);
                    }
                });
                alertadd.show();
            }
        });

        btnCarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restanteManejo();
                banderaConducir = true;
                btnContinuar.setImageResource(R.drawable.conducir_off);
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                int validaInternet = verificarSenal();
                conducir = "" + banderaConducir;
                onDuty = "" + banderaOnDuty;
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    try {
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                        if (addresses.size() > 0) {
                            String address1 = addresses.get(0).getAddressLine(0);
                            direccion = address1;
                            System.out.println("dirección solo coordenadas: " + address1);
                        }
                    } catch (IOException e) {
                        Log.e("tag", e.getMessage());
                    }
                    boolean isInserted = bitacoraDb.insertData(2, 5, 2, currentDate,
                            currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1,"00:00:00", "00:00:00", conducir, onDuty, 1);
                    if(isInserted = true){
                        tituloNoti.setText("Actividades Auxiliares");
                        tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                        chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                        BanderaTiempo = 1;
                        Tiempo();
                    }else{
                        Toast.makeText(getApplicationContext(), "notificación carga no insertada correctamente.", Toast.LENGTH_LONG).show();
                    }
                    accederPermisoLocation(MsgTexto[9], 1);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                        boolean isInserted = bitacoraDb.insertData(2, 5, 2, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0,"00:00:00", "00:00:00", conducir, onDuty, 1);
                        if(isInserted = true){
                            tituloNoti.setText("Actividades Auxiliares");
                            tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                            chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                            BanderaTiempo = 1;
                            Tiempo();
                        }else{
                            Toast.makeText(getApplicationContext(), "notificación carga no insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                final View view1 = factory.inflate(R.layout.image_assistant, null);
                final ImageView dialogImageView = (ImageView) view1.findViewById(R.id.iv);
                dialogImageView.setImageResource(R.drawable.happy_assistant);
                alertadd.setView(view1);
                alertadd.setTitle("¡Buen trabajo "+ strNombre + "!");
                alertadd.setMessage("Las cargas y entregas a tiempo hablan bien de todos nosotros! Cuando concluyas presiona continuar en el icono del volante.");
                alertadd.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int sumthin) {
                    }
                });
                alertadd.show();
            }
        });

        btnDescarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restanteManejo();
                banderaConducir = true;
                btnContinuar.setImageResource(R.drawable.conducir_off);
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                int validaInternet = verificarSenal();
                conducir = "" + banderaConducir;
                onDuty = "" + banderaOnDuty;
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    try {
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                        if (addresses.size() > 0) {
                            String address1 = addresses.get(0).getAddressLine(0);
                            direccion = address1;
                            System.out.println("dirección solo coordenadas: " + address1);
                        }
                    } catch (IOException e) {
                        Log.e("tag", e.getMessage());
                    }
                    boolean isInserted = bitacoraDb.insertData(2, 6, 2, currentDate,
                            currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1,"00:00:00", "00:00:00", conducir, onDuty, 1);
                    if(isInserted = true){
                        tituloNoti.setText("Actividades Auxiliares");
                        tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                        chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                        BanderaTiempo = 1;
                        Tiempo();
                    }else{
                        Toast.makeText(getApplicationContext(), "notificación entrega insertada correctamente.", Toast.LENGTH_LONG).show();
                    }
                    accederPermisoLocation(MsgTexto[10], 1);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                        boolean isInserted = bitacoraDb.insertData(2, 6, 2, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0,"00:00:00", "00:00:00", conducir, onDuty, 1);
                        if(isInserted = true){
                            tituloNoti.setText("Actividades Auxiliares");
                            tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                            chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                            BanderaTiempo = 1;
                            Tiempo();
                        }else{
                            Toast.makeText(getApplicationContext(), "notificación entrega insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                final View view1 = factory.inflate(R.layout.image_assistant, null);
                final ImageView dialogImageView = (ImageView) view1.findViewById(R.id.iv);
                dialogImageView.setImageResource(R.drawable.happy_assistant);
                alertadd.setView(view1);
                alertadd.setTitle("¡Buen trabajo "+ strNombre + "!");
                alertadd.setMessage("Las cargas y entregas a tiempo hablan bien de todos nosotros! Cuando concluyas presiona continuar en el icono del volante.");
                alertadd.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int sumthin) {
                    }
                });
                alertadd.show();
            }
        });

        btnMatt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restanteManejo();
                banderaConducir = true;
                btnContinuar.setImageResource(R.drawable.conducir_off);
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                int validaInternet = verificarSenal();
                conducir = "" + banderaConducir;
                onDuty = "" + banderaOnDuty;
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    try {
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                        if (addresses.size() > 0) {
                            String address1 = addresses.get(0).getAddressLine(0);
                            direccion = address1;
                            System.out.println("dirección solo coordenadas: " + address1);
                        }
                    } catch (IOException e) {
                        Log.e("tag", e.getMessage());
                    }
                    boolean isInserted = bitacoraDb.insertData(2, 8, 2, currentDate,
                            currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1,"00:00:00", "00:00:00", conducir, onDuty, 1);
                    if(isInserted = true){
                        tituloNoti.setText("Actividades Auxiliares");
                        tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                        chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                        BanderaTiempo = 1;
                        Tiempo();
                    }else{
                        Toast.makeText(getApplicationContext(), "notificación matt no insertada correctamente.", Toast.LENGTH_LONG).show();
                    }
                    accederPermisoLocation(MsgTexto[14], 1);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                        boolean isInserted = bitacoraDb.insertData(2, 8, 2, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0,"00:00:00", "00:00:00", conducir, onDuty, 1);
                        if(isInserted = true){
                            tituloNoti.setText("Actividades Auxiliares");
                            tituloNoti.setTextColor(Color.parseColor("#f5d231"));
                            chrChronometer.setTextColor(Color.parseColor("#f5d231"));
                            BanderaTiempo = 1;
                            Tiempo();
                        }else{
                            Toast.makeText(getApplicationContext(), "notificación matt no insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                final View view1 = factory.inflate(R.layout.image_assistant, null);
                final ImageView dialogImageView = (ImageView) view1.findViewById(R.id.iv);
                dialogImageView.setImageResource(R.drawable.happy_assistant);
                alertadd.setView(view1);
                alertadd.setTitle("¡Buen trabajo "+ strNombre + "!");
                alertadd.setMessage("Las cargas y entregas a tiempo hablan bien de todos nosotros! Cuando concluyas presiona continuar en el icono del volante.");
                alertadd.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int sumthin) {
                    }
                });
                alertadd.show();
            }
        });

        btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                if(banderaConducir == false){
                    countDownTimer.cancel();
                    banderaConducir = true;
                    btnContinuar.setImageResource(R.drawable.conducir_off);
                    Toast.makeText(getApplicationContext(), "Conductor no está conduciendo.", Toast.LENGTH_LONG).show();

                    int validaInternet = verificarSenal();
                    conducir = "" + banderaConducir;
                    onDuty = "" + banderaOnDuty;
                    if (validaInternet == 1) {
                        locationTrack = new LocationTrack(HomeActivity.this);
                        if (locationTrack.canGetLocation()) {
                            double longitude = locationTrack.getLongitude();
                            double latitude = locationTrack.getLatitude();
                            LongData = longitude;
                            LatData = latitude;
                        } else {
                            locationTrack.showSettingsAlert();
                        }
                        try {
                            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                            if (addresses.size() > 0) {
                                String address1 = addresses.get(0).getAddressLine(0);
                                direccion = address1;
                                System.out.println("dirección solo coordenadas: " + address1);
                            }
                        } catch (IOException e) {
                            Log.e("tag", e.getMessage());
                        }
                        boolean isInserted = bitacoraDb.insertData(7, 18, 4, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1,"00:00:00", "00:00:00", conducir, onDuty, 0);
                        if(isInserted = true){
                            tituloNoti.setText("Sin Conducir");
                            tituloNoti.setTextColor(Color.parseColor("#c6c6c6"));
                            dhrChronometer.setTextColor(Color.parseColor("#c6c6c6"));
                            BanderaTiempo = 3;
                            Tiempo();
                        }else{
                            Toast.makeText(getApplicationContext(), "notificación sin conducir no insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                        accederPermisoLocation(MsgTexto[12], 1);
                    } else {
                        if (validaInternet == 0) {
                            Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                            boolean isInserted = bitacoraDb.insertData(7, 18, 4, currentDate,
                                    currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0,"00:00:00", "00:00:00", conducir, onDuty, 0);
                            if(isInserted = true){
                                tituloNoti.setText("Sin Conducir");
                                tituloNoti.setTextColor(Color.parseColor("#c6c6c6"));
                                dhrChronometer.setTextColor(Color.parseColor("#c6c6c6"));
                                BanderaTiempo = 3;
                                Tiempo();
                            }else{
                                Toast.makeText(getApplicationContext(), "notificación sin conducir no insertada correctamente.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }else{
                    restanteManejo();
                    banderaConducir = false;
                    btnContinuar.setImageResource(R.drawable.conduciendo_notidriver);
                    int validaInternet1 = verificarSenal();
                    conducir = "" + banderaConducir;
                    onDuty = "" + banderaOnDuty;
                    if (validaInternet1 == 1) {
                        locationTrack = new LocationTrack(HomeActivity.this);
                        if (locationTrack.canGetLocation()) {
                            double longitude = locationTrack.getLongitude();
                            double latitude = locationTrack.getLatitude();
                            LongData = longitude;
                            LatData = latitude;
                        } else {
                            locationTrack.showSettingsAlert();
                        }
                        try {
                            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            List<Address> addresses = geocoder.getFromLocation(LatData, LongData, 1);
                            if (addresses.size() > 0) {
                                String address1 = addresses.get(0).getAddressLine(0);
                                direccion = address1;
                                System.out.println("dirección solo coordenadas: " + address1);
                            }
                        } catch (IOException e) {
                            Log.e("tag", e.getMessage());
                        }
                        boolean isInserted = bitacoraDb.insertData(1, 17, 4, currentDate,
                                currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 1,"00:00:00", "00:00:00", conducir, onDuty, 0);
                        if(isInserted = true){
                            tituloNoti.setText("Conduciendo");
                            tituloNoti.setTextColor(Color.parseColor("#2ddb12"));
                            chrChronometer.setTextColor(Color.parseColor("#2ddb12"));
                            BanderaTiempo = 1;
                            Tiempo();
                        }else{
                            Toast.makeText(getApplicationContext(), "notificación conduciendo no insertada correctamente.", Toast.LENGTH_LONG).show();
                        }
                        accederPermisoLocation(MsgTexto[11], 1);
                    } else {
                        if (validaInternet1 == 0) {
                            Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                            boolean isInserted = bitacoraDb.insertData(1, 17, 4, currentDate,
                                    currentTime, currentTime, "00:00:00", direccion, idTelematics, idUsuario, 0, 0,"00:00:00", "00:00:00", conducir, onDuty, 0);
                            if(isInserted = true){
                                tituloNoti.setText("Conduciendo");
                                tituloNoti.setTextColor(Color.parseColor("#2ddb12"));
                                chrChronometer.setTextColor(Color.parseColor("#2ddb12"));
                                BanderaTiempo = 1;
                                Tiempo();
                            }else{
                                Toast.makeText(getApplicationContext(), "notificación conduciendo no insertada correctamente.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
            }
        });
        /************************************************************************************************/
    }

    private int verificarSenal() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return 1;
        } else {
            return 0;
        }
    }

    private void accederPermisoLocation(final String mensaje, final int validaActividad) {

        //si la API 23 a mas
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Habilitar permisos para la version de API 23 a mas

            int verificarPermisoLocationCoarse = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int verificarPermisoLocationFine = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            //Verificamos si el permiso no existe
            if (verificarPermisoLocationCoarse != PackageManager.PERMISSION_GRANTED && verificarPermisoLocationFine != PackageManager.PERMISSION_GRANTED) {
                //verifico si el usuario a rechazado el permiso anteriormente
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION) && shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Si a rechazado el permiso anteriormente muestro un mensaje
                    mostrarExplicacion();
                } else {
                    //De lo contrario carga la ventana para autorizar el permiso
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                }
            } else {
                //Si el permiso fue activado llamo al metodo de ubicacion
                fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            linkLocation[0] = "http://maps.google.com/maps?q=loc:" + location.getLatitude() + "," + location.getLongitude();
                            System.out.println("coordenadas enviadas a telematics: " + location.getLatitude() + ", " + location.getLongitude());
                            System.out.println("coordenadas enviadas a telematics: " + LatData + ", " + LongData);
                            new HomeActivity.registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,recibeUnidad,String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()),mensaje);
                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                            Message message = new Message()
                                    .time(new Date().getTime())
                                    .location(new com.gurtam.wiatagkit.Location(location.getLatitude(), location.getLongitude(), location.getAltitude(), location.getSpeed(), (short) location.getBearing(), (byte)location.getAccuracy()))
                                    .batteryLevel((byte)batLevel)
                                    .text("AVISO: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");
                            System.out.println("mensaje gurtam: " +  message);
                            MessageSender.sendMessage(message, new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Notificación enviada con éxito a monitoreo.\n" + mensaje, Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío de la notificación.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }); //método de envío de mensaje a wialon.
                        }else{
                            new HomeActivity.registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,recibeUnidad,"0","0",mensaje);

                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

                            Message message = new Message()
                                    .time(new Date().getTime())
                                    //.location(new com.gurtam.wiatagkit.Location(LatData, LongData,0.0, 10,(short) 1,(byte)1))
                                    .batteryLevel((byte)batLevel)
                                    .text("AVISO: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");
                            MessageSender.sendMessage(message,new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Notificación enviada con éxito a monitoreo.\n"+mensaje,Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío de la notificación.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
        } else {//Si la API es menor a 23 - llamo al metodo de uFbicacion
            fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        linkLocation[0] = "http://maps.google.com/maps?q=loc:"+location.getLatitude()+","+location.getLongitude();
                        new HomeActivity.registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,recibeUnidad,String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()),mensaje);
                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        Message message = new Message()
                                .time(new Date().getTime())
                                .location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                .batteryLevel((byte)batLevel)
                                .text("AVISO: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");
                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Notificación enviada con éxito a monitoreo.\n"+mensaje,Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío de la notificación.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                    }
                    else{
                        new HomeActivity.registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,recibeUnidad,"0","0",mensaje);
                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        Message message = new Message()
                                .time(new Date().getTime())
                                //.location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                .batteryLevel((byte)batLevel)
                                .text("AVISO: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");
                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Notificación enviada con éxito a monitoreo.\n"+mensaje,Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío de la notificación.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
    }

    private void mostrarExplicacion() {
        new AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para acceder a la ultima y actual ubicación del dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    public void mensajeAccionCancelada() {
        Toast.makeText(getApplicationContext(), "Haz rechazado la petición, puede suceder que la app no trabaje de manera adecuada.", Toast.LENGTH_SHORT).show();
    }

    public class registerEvent extends AsyncTask<String, Void, String>{

        String saveResponse = "";

        @Override
        protected String doInBackground(String... strings) {
            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("unit_id", strings[0]);
                paramObject.put("unit_x", strings[1]);
                paramObject.put("unit_y", strings[2]);
                paramObject.put("unit_txt", strings[3]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();
            client.connectTimeoutMillis();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/add/event")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jo = new JSONObject(result);
                String success = jo.getString("success");

                if(success != "false"){
                    //Toast.makeText(getApplicationContext(), "Notificación registrada exitosamente", Toast.LENGTH_LONG).show();
                }else{
                    String msg = jo.getString("message");
                    //Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                Toast.makeText(getBaseContext(),"Error en el servidor, para registro de evento.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
    }


    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(HomeActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void AlertNoGps(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¡GPS desactivado, favor de activarlo!")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });
        alert = builder.create();
        alert.show();
    }

    private class setDataTask extends AsyncTask<String, Void, String> {

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(getApplicationContext(),"Sincronizando datos...",Toast.LENGTH_LONG).show();
        }

        @Override
        protected String doInBackground(String... params) {
            System.out.println("Params: " + params);
            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("estatus", params[0]);
                paramObject.put("descripcion", params[1]);
                paramObject.put("tipo", params[2]);
                paramObject.put("fecha", params[3]);
                paramObject.put("hora_inicio", params[4]);
                paramObject.put("hora_final", params[5]);
                paramObject.put("duracion", params[6]);
                paramObject.put("ubicacion", params[7]);
                paramObject.put("rid_telematics", params[8]);
                paramObject.put("id_usuario", params[9]);
                paramObject.put("Online", params[10]);
                System.out.println(params[10]);
                System.out.println(params[11]);
                idSync = params[11];
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/insertBitacora")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                if(response.code() == 404){
                    saveResponse =  response.body().string().toString();
                }else {
                    if(response.code() == 400){
                        saveResponse =  response.body().string().toString();
                    }else{
                        if(response.isSuccessful()){
                            saveResponse =  response.body().string().toString();
                        }else{
                            saveResponse = response.body().string().toString();
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("id de sincronizacion " + idSync);
            int updateIdSync = Integer.parseInt(String.valueOf(idSync));
            boolean isUpdate = bitacoraDb.updateDataSync(updateIdSync, 1);
            if(isUpdate  == true){
                System.out.println("dato actualizado correctamente y sincronizado." + updateIdSync);
            }else{
                System.out.println("dato no actualizado ni sincronizado." + updateIdSync);
            }
            return saveResponse;
        }

        protected void onProgressUpdate(Integer... values) {
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                if(result.isEmpty()){
                    String msg = "Notificaciones insertadas correctamente.";
                }else{
                    JSONObject jo = new JSONObject(result);
                    String success = jo.getString("success");

                    if (success != "false") {
                        Toast.makeText(getApplicationContext(),"¡Notificación insertada correctamente!",Toast.LENGTH_LONG).show();
                    } else {
                        String msg = jo.getString("message");
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                Toast.makeText(getApplicationContext(),"Upss, ocurrio un error intentalo de nuevo.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

        }

    }

    public void Onduty(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[12], 1);
    }

    public void Offduty(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[16], 1);
    }

    public void Comida(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[0], 1);
    }

    public void WC(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[1], 2);
    }

    public void Diesel(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[2], 3);
    }

    public void Descanso(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[3], 4);
    }

    public void Clima(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[4], 5);
    }

    public void Averia(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[5], 6);
    }

    public void Reten(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[6], 7);
    }

    public void Trafico(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[7], 8);
    }

    public void SOS(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[8], 9);
    }

    public void Carga(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[9], 1);
    }

    public void Descarga(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[10], 9);
    }
    public void Pausa(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[13], 9);
    }
    public void Matt(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[14], 9);
    }

    public void Conduciendo(){
        locationTrack = new LocationTrack(HomeActivity.this);
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
            LongData = longitude;
            LatData = latitude;
        } else {
            locationTrack.showSettingsAlert();
        }
        accederPermisoLocation(MsgTexto[11], 1);
    }

    public void Tiempo(){
        String data = null, data1 = null, dataid = null, dataid2 = null, fecha = null, fecha1 = null, dateUltimo = null, datePenultimo=null;
        Cursor res = bitacoraDb.ultimoBitacora();
        if(res.getCount() == 0){
            System.out.println("Sin datos");
            data = "00:00:00";
            fecha = "0000-00-00";
            dateUltimo = fecha + " " + data;
        }
        while(res.moveToNext()){
            dataid = res.getString(0);
            data = res.getString(1);
            fecha = res.getString(2);
            dateUltimo = fecha + " " + data;
            System.out.println("ultimo registro: " + data + ", " + dataid + ", " + fecha + ", " + dateUltimo);
        }
        Cursor res1 = bitacoraDb.penultimoBitacora();
        if(res1.getCount() == 0){
            System.out.println("Sin datos");
            datePenultimo = dateUltimo;
            System.out.println("penultimo registro: " + datePenultimo);
        }
        while(res1.moveToNext()){
            dataid2 = res1.getString(0);
            data1 = res1.getString(1);
            fecha1 = res1.getString(2);
            datePenultimo = fecha1 + " " + data1;
            System.out.println("penultimo registro: " + data1 + ", " + dataid2 + ", " + fecha1 + ", " + datePenultimo);
        }
        try {

            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = sdf.parse(dateUltimo);
            DateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date1 = sdf1.parse(datePenultimo);
            long diff = date.getTime() - date1.getTime();
            System.out.println("diff: " + diff);

            if(BanderaTiempo==1 && tipoNotificacion == true){
                dhrChronometer.setVisibility(View.INVISIBLE);
                dhrChronometer.setBase(SystemClock.elapsedRealtime());
                chrChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener(){
                    @Override
                    public void onChronometerTick(Chronometer cArg) {
                        long time = SystemClock.elapsedRealtime() - cArg.getBase();
                        int h   = (int)(time /3600000);
                        int m = (int)(time - h*3600000)/60000;
                        int s= (int)(time - h*3600000- m*60000)/1000 ;
                        String hh = h < 10 ? "0"+h: h+"";
                        String mm = m < 10 ? "0"+m: m+"";
                        String ss = s < 10 ? "0"+s: s+"";
                        cArg.setText(hh+":"+mm+":"+ss);
                    }
                });
                chrChronometer.setBase(SystemClock.elapsedRealtime());
                chrChronometer.start();
                chrChronometer.setVisibility(View.VISIBLE);
                String conducc= chrtmp.getText().toString() ;
                String[] tokens = conducc.split(":");
                int secondsToMs = Integer.parseInt(tokens[2]) * 1000;
                int minutesToMs = Integer.parseInt(tokens[1]) * 60000;
                int hoursToMs = Integer.parseInt(tokens[0]) * 3600000;
                long total = secondsToMs + minutesToMs + hoursToMs;
                System.out.println("AQUI"+ total);

                long condsum = diff + total;
                String hms1 = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(condsum),
                        TimeUnit.MILLISECONDS.toMinutes(condsum) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(condsum)),
                        TimeUnit.MILLISECONDS.toSeconds(condsum) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(condsum)));
                chrtmp.setText(hms1);
                System.out.println("AQUI"+condsum);
                tipoNotificacion = true;

                String duracion2 = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(diff),
                        TimeUnit.MILLISECONDS.toMinutes(diff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(diff)),
                        TimeUnit.MILLISECONDS.toSeconds(diff) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(diff)));

                int updateAcumulado = Integer.parseInt(String.valueOf(dataid));
                boolean isUpdate = bitacoraDb.updateAcumulado(updateAcumulado, dhrtmp.getText().toString(), chrtmp.getText().toString(), duracion2);
                if(isUpdate  == true){
                    System.out.println("dato actualizado correctamente y sincronizado." + updateAcumulado);
                }else{
                    System.out.println("dato no actualizado ni sincronizado." + updateAcumulado);
                }
            }else if(BanderaTiempo==3 && tipoNotificacion == false){
                chrChronometer.setVisibility(View.INVISIBLE);
                chrChronometer.setBase(SystemClock.elapsedRealtime());

                dhrChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener(){
                    @Override
                    public void onChronometerTick(Chronometer cArg) {
                        long time = SystemClock.elapsedRealtime() - cArg.getBase();
                        int h   = (int)(time /3600000);
                        int m = (int)(time - h*3600000)/60000;
                        int s= (int)(time - h*3600000- m*60000)/1000 ;
                        String hh = h < 10 ? "0"+h: h+"";
                        String mm = m < 10 ? "0"+m: m+"";
                        String ss = s < 10 ? "0"+s: s+"";
                        cArg.setText(hh+":"+mm+":"+ss);
                    }
                });
                dhrChronometer.setBase(SystemClock.elapsedRealtime());
                dhrChronometer.start();
                dhrChronometer.setVisibility(View.VISIBLE);
                String desc = dhrtmp.getText().toString() ;
                String[] tokens1 = desc.split(":");
                int secondsToMs1 = Integer.parseInt(tokens1[2]) * 1000;
                int minutesToMs1 = Integer.parseInt(tokens1[1]) * 60000;
                int hoursToMs1 = Integer.parseInt(tokens1[0]) * 3600000;
                long total1 = secondsToMs1 + minutesToMs1 + hoursToMs1;
                System.out.println("AQUI"+ total1);
                System.out.println(diff);
                long condsum1 = diff + total1;
                String hms2 = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(condsum1),
                        TimeUnit.MILLISECONDS.toMinutes(condsum1) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(condsum1)),
                        TimeUnit.MILLISECONDS.toSeconds(condsum1) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(condsum1)));
                dhrtmp.setText(hms2);
                System.out.println(condsum1);
                tipoNotificacion = false;

                String duracion = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(diff),
                        TimeUnit.MILLISECONDS.toMinutes(diff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(diff)),
                        TimeUnit.MILLISECONDS.toSeconds(diff) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(diff)));

                int updateAcumulado = Integer.parseInt(String.valueOf(dataid));
                boolean isUpdate = bitacoraDb.updateAcumulado(updateAcumulado, dhrtmp.getText().toString(), chrtmp.getText().toString(), duracion);

                if(isUpdate  == true){
                    System.out.println("dato actualizado correctamente y sincronizado." + updateAcumulado);
                }else{
                    System.out.println("dato no actualizado ni sincronizado." + updateAcumulado);
                }
            } else if(BanderaTiempo==1 && tipoNotificacion == false){
                dhrChronometer.setVisibility(View.INVISIBLE);
                dhrChronometer.setBase(SystemClock.elapsedRealtime());
                chrChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener(){
                    @Override
                    public void onChronometerTick(Chronometer cArg) {
                        long time = SystemClock.elapsedRealtime() - cArg.getBase();
                        int h   = (int)(time /3600000);
                        int m = (int)(time - h*3600000)/60000;
                        int s= (int)(time - h*3600000- m*60000)/1000 ;
                        String hh = h < 10 ? "0"+h: h+"";
                        String mm = m < 10 ? "0"+m: m+"";
                        String ss = s < 10 ? "0"+s: s+"";
                        cArg.setText(hh+":"+mm+":"+ss);
                    }
                });
                chrChronometer.setBase(SystemClock.elapsedRealtime());
                chrChronometer.start();
                chrChronometer.setVisibility(View.VISIBLE);
                String desc1 = dhrtmp.getText().toString() ;
                String[] tokens1 = desc1.split(":");
                int secondsToMs2 = Integer.parseInt(tokens1[2]) * 1000;
                int minutesToMs2 = Integer.parseInt(tokens1[1]) * 60000;
                int hoursToMs2 = Integer.parseInt(tokens1[0]) * 3600000;
                long total2 = secondsToMs2 + minutesToMs2 + hoursToMs2;
                System.out.println("AQUI"+ total2);
                System.out.println(diff);
                long condsum2 = diff + total2;
                String hms3 = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(condsum2),
                        TimeUnit.MILLISECONDS.toMinutes(condsum2) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(condsum2)),
                        TimeUnit.MILLISECONDS.toSeconds(condsum2) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(condsum2)));
                dhrtmp.setText(hms3);
                System.out.println(condsum2);
                tipoNotificacion = true;

                String duracion1 = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(diff),
                        TimeUnit.MILLISECONDS.toMinutes(diff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(diff)),
                        TimeUnit.MILLISECONDS.toSeconds(diff) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(diff)));

                int updateAcumulado = Integer.parseInt(String.valueOf(dataid));
                boolean isUpdate = bitacoraDb.updateAcumulado(updateAcumulado, dhrtmp.getText().toString(), chrtmp.getText().toString(), duracion1);
                if(isUpdate  == true){
                    System.out.println("dato actualizado correctamente y sincronizado." + updateAcumulado);
                }else{
                    System.out.println("dato no actualizado ni sincronizado." + updateAcumulado);
                }
            }else if(BanderaTiempo==3 && tipoNotificacion == true){
                chrChronometer.setVisibility(View.INVISIBLE);
                chrChronometer.setBase(SystemClock.elapsedRealtime());
                dhrChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener(){
                    @Override
                    public void onChronometerTick(Chronometer cArg) {
                        long time = SystemClock.elapsedRealtime() - cArg.getBase();
                        int h   = (int)(time /3600000);
                        int m = (int)(time - h*3600000)/60000;
                        int s= (int)(time - h*3600000- m*60000)/1000 ;
                        String hh = h < 10 ? "0"+h: h+"";
                        String mm = m < 10 ? "0"+m: m+"";
                        String ss = s < 10 ? "0"+s: s+"";
                        cArg.setText(hh+":"+mm+":"+ss);
                    }
                });
                dhrChronometer.setBase(SystemClock.elapsedRealtime());
                dhrChronometer.start();
                dhrChronometer.setVisibility(View.VISIBLE);
                String desc1 = chrtmp.getText().toString() ;
                String[] tokens3 = desc1.split(":");
                int secondsToMs3 = Integer.parseInt(tokens3[2]) * 1000;
                int minutesToMs3 = Integer.parseInt(tokens3[1]) * 60000;
                int hoursToMs3 = Integer.parseInt(tokens3[0]) * 3600000;
                long total3 = secondsToMs3 + minutesToMs3 + hoursToMs3;
                System.out.println("AQUI"+ total3);
                System.out.println(diff);
                long condsum3 = diff + total3;
                String hms4 = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(condsum3),
                        TimeUnit.MILLISECONDS.toMinutes(condsum3) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(condsum3)),
                        TimeUnit.MILLISECONDS.toSeconds(condsum3) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(condsum3)));
                chrtmp.setText(hms4);
                System.out.println(condsum3);
                tipoNotificacion = false;

                String duracion3 = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(diff),
                        TimeUnit.MILLISECONDS.toMinutes(diff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(diff)),
                        TimeUnit.MILLISECONDS.toSeconds(diff) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(diff)));

                int updateAcumulado = Integer.parseInt(String.valueOf(dataid));
                boolean isUpdate = bitacoraDb.updateAcumulado(updateAcumulado, dhrtmp.getText().toString(), chrtmp.getText().toString(), duracion3);

                if(isUpdate  == true){
                    System.out.println("dato actualizado correctamente y sincronizado." + updateAcumulado);
                }else{
                    System.out.println("dato no actualizado ni sincronizado." + updateAcumulado);
                }
        }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    public void restanteManejo(){
        if(countDownTimer!=null){
            countDownTimer.cancel();
        }

        final String[] milliseconds = {""};

        String descansoHex = dhrtmp.getText().toString();
        String[] tokens1 = descansoHex.split(":");
        int secondsToM = Integer.parseInt(tokens1[2]) * 1000;
        int minutesToM = Integer.parseInt(tokens1[1]) * 60000;
        int hoursToM = Integer.parseInt(tokens1[0]) * 3600000;
        long totalD = secondsToM + minutesToM + hoursToM;
        System.out.println("milisegundos"+ totalD);

        String conducc= chrtmp.getText().toString() ;
        String[] tokens = conducc.split(":");
        int secondsToMs = Integer.parseInt(tokens[2]) * 1000;
        int minutesToMs = Integer.parseInt(tokens[1]) * 60000;
        int hoursToMs = Integer.parseInt(tokens[0]) * 3600000;
        long total = secondsToMs + minutesToMs + hoursToMs;
        System.out.println("AQUI"+ total);

        driveTime = Integer.parseInt(String.valueOf(total));

        restante = delayTimer - driveTime;

        countDownTimer = new CountDownTimer(restante, 1000) {

            public void onTick(long millisUntilFinished) {
                mTextField.setText("" + millisUntilFinished / 1000);
                String data = mTextField.getText().toString();
                System.out.println("Tiempo: " + data);
                if(data.equals("18000")){
                    if(totalD <= 1800000) {
                        createNotificationChannel();
                        createNotification();
                        System.out.println("Primer descanso" + data);
                        mTextField.setText("PRIMER DESCANSO!");
                        AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                        LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                        final View view = factory.inflate(R.layout.image_assistant, null);
                        final ImageView dialogImageView = (ImageView) view.findViewById(R.id.iv);
                        dialogImageView.setImageResource(R.drawable.coffee_assistant);
                        alertadd.setView(view);
                        alertadd.setTitle("¡ATENCIÓN EXCESO DE MANEJO!");
                        alertadd.setMessage("Haz una pausa y/o toma un descanso.");
                        alertadd.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dlg, int sumthin) {
                            }
                        });
                        alertadd.show();
                    } else{
                        Toast.makeText(getApplicationContext(), "Continuando con el viaje", Toast.LENGTH_LONG).show();
                    }
                }
                if(data.equals("36000")){
                    if(totalD <= 3600000) {
                        createNotificationChannel();
                        createNotification();
                        System.out.println("segundo descanso" + data);
                        mTextField.setText("SEGUNDO DESCANSO!");
                        AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                        LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                        final View view = factory.inflate(R.layout.image_assistant, null);
                        final ImageView dialogImageView = (ImageView) view.findViewById(R.id.iv);
                        dialogImageView.setImageResource(R.drawable.coffee_assistant);
                        alertadd.setView(view);
                        alertadd.setTitle("¡ATENCIÓN EXCESO DE MANEJO!");
                        alertadd.setMessage("Haz una pausa y/o toma un descanso.");
                        alertadd.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dlg, int sumthin) {
                            }
                        });
                        alertadd.show();
                    }else{
                        Toast.makeText(getApplicationContext(), "Continuando con el viaje", Toast.LENGTH_LONG).show();
                    }
                }
            }

            public void onFinish() {
                mTextField.setText("TIEMPO DE MANEJO TERMINADO!");
                createNotificationChannel();
                createNotification();
                AlertDialog.Builder alertadd = new AlertDialog.Builder(HomeActivity.this);
                LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
                final View view = factory.inflate(R.layout.image_assistant, null);
                final ImageView dialogImageView = (ImageView) view.findViewById(R.id.iv);
                dialogImageView.setImageResource(R.drawable.coffee_assistant);
                alertadd.setView(view);
                alertadd.setCancelable(false);
                alertadd.setTitle("¡ATENCIÓN EXCESO DE MANEJO!");
                alertadd.setMessage("TOMA UN DESCANSO DE 8 HORAS PARA CONTINUAR.");
                alertadd.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int sumthin) {
                        conduccionTerminada = true;
                    }
                });
                alertadd.show();
            }
        }.start();
    }

    public void sincronizacion(){
        int validaInternet = verificarSenal();
        if (validaInternet == 1) {
            System.out.println("Sincronizando datos...");
            handler.postDelayed(runnable, delay);
            Cursor res = bitacoraDb.getAllData();
            if (res.getCount() == 0) {
                return;
            }
            while (res.moveToNext()) {
                String dataId = res.getString(0);
                String data = res.getString(1);
                String data1 = res.getString(2);
                String data2 = res.getString(3);
                String data3 = res.getString(4);
                String data4 = res.getString(5);
                String data5 = res.getString(6);
                String data6 = res.getString(7);
                String data7 = res.getString(8);
                String data8 = res.getString(9);
                String data9 = res.getString(10);
                String data10 = res.getString(12);
                System.out.println("datos: " + data + ", " + data1 + ", " + data2 + ", " + data3 + ", " + data4 + ", " + data5 + ", " + data6 + ", " + data7 + ", " + data8 + ", " + data9 + ", " + data10 + ", id: " + dataId);
                SystemClock.sleep(2000);
                new setDataTask().execute(data, data1, data2, data3, data4, data5, data6, data7, data8, data9, data10, dataId);
            }

            Cursor offline = bitacoraDb.getDataOffline();
            if (offline.getCount() == 0) {
                return;
            }

            while (offline.moveToNext()) {
                String IdUpdate = offline.getString(0);
                String dataOff = offline.getString(1);
                String data1Off = offline.getString(2);
                String data2Off = offline.getString(3);
                String data3Off = offline.getString(4);
                String data4Off = offline.getString(5);
                String data5Off = offline.getString(6);
                String data6Off = offline.getString(7);
                String data7Off = offline.getString(8);
                String data8Off = offline.getString(9);
                String data9Off = offline.getString(10);
                String data10Off = offline.getString(12);


                System.out.println("datos para enviar notificación: " + dataOff + ", " + data1Off + ", " + data2Off + ", " + data3Off + ", " + data4Off + ", " + data5Off + ", " + data6Off + ", " + data7Off + ", " + data8Off + ", " + data9Off + ", " + data10Off + "  -- " + IdUpdate);

                String enviaOffline = data1Off;

                int updateId = Integer.parseInt(String.valueOf(IdUpdate));

                if (enviaOffline.equals("1")) {
                    Pausa();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                } else if (enviaOffline.equals("2")) {
                    Comida();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                } else if (enviaOffline.equals("3")) {
                    WC();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                } else if (enviaOffline.equals("4")) {
                    Descanso();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                } else if (enviaOffline.equals("5")) {
                    Carga();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                } else if (enviaOffline.equals("6")) {
                    Descarga();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                } else if (enviaOffline.equals("7")) {
                    Diesel();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                } else if (enviaOffline.equals("8")) {
                    Matt();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                } else if (enviaOffline.equals("9")) {
                    Averia();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                } else if (enviaOffline.equals("10")) {
                    Reten();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                } else if (enviaOffline.equals("11")) {
                    Clima();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                } else if (enviaOffline.equals("12")) {
                    Trafico();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                } else if (enviaOffline.equals("13")) {
                    Onduty();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                } else if (enviaOffline.equals("14")) {
                    Offduty();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                } else if (enviaOffline.equals("16")) {
                    SOS();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                } else if (enviaOffline.equals("17")) {
                    Conduciendo();
                    boolean isUpdate = bitacoraDb.updateData(updateId, 1);
                    if (isUpdate == true) {
                        System.out.println("dato actualizado correctamente" + updateId);
                    } else {
                        System.out.println("dato no actualizado" + updateId);
                    }
                }
            }
        } else {
            if (validaInternet == 0) {
                Toast.makeText(HomeActivity.this, "Perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void validaReinicio(){
        String descansoValida = dhrtmp.getText().toString();
        String[] tokens1 = descansoValida.split(":");
        int secondsToM = Integer.parseInt(tokens1[2]) * 1000;
        int minutesToM = Integer.parseInt(tokens1[1]) * 60000;
        int hoursToM = Integer.parseInt(tokens1[0]) * 3600000;
        long totalD = secondsToM + minutesToM + hoursToM;
        System.out.println("milisegundos"+ totalD);
        System.out.println("error"+ conduccionTerminada);

        String conduccionValida = chrtmp.getText().toString();
        String[] tokens = conduccionValida.split(":");
        int secondsTo = Integer.parseInt(tokens[2]) * 1000;
        int minutesTo = Integer.parseInt(tokens[1]) * 60000;
        int hoursTo = Integer.parseInt(tokens[0]) * 3600000;
        long totalC = secondsTo + minutesTo + hoursTo;
        System.out.println("milisegundos"+ totalC);

        if(totalC <= 50400000){
            if(totalD >= 32400000){
                deleteDataBitacora();
            }
        } else if(totalC <= 100800000){
            if(totalD >= 64800000){
                deleteDataBitacora();
            }
        } else if(totalC <= 151200000){
            if(totalD >= 97200000){
                deleteDataBitacora();
            }
        } else if(totalC <= 201600000){
            if(totalD >= 129600000){
                deleteDataBitacora();
            }
        }
    }

    public void deleteDataBitacora(){
        Integer deleteAll = bitacoraDb.deleteData();
        if(deleteAll > 0){
            Toast.makeText(getApplicationContext(), "Reiniciando temporizadores...", Toast.LENGTH_LONG).show();
            chrtmp.setText("00:00:00");
            dhrtmp.setText("00:00:00");
        }else{
            Toast.makeText(getApplicationContext(), "Temporizador sin reiniciar...", Toast.LENGTH_LONG).show();
        }
    }

    public void finalizando(){
        sincronizacion();
        validaReinicio();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Notificación";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void createNotification(){
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID);
        Vibrator v = (Vibrator) this.getApplication().getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        v.vibrate(10000);
        builder.setSmallIcon(R.drawable.ic_baseline_warning_24);
        builder.setContentTitle("¡ATENCIÓN: EXCESO DE MANEJO");
        builder.setContentText("Haz una pausa y/o toma un descanso.");
        builder.setColor(Color.RED);
        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        builder.setDefaults(Notification.DEFAULT_SOUND);
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getApplicationContext());
        notificationManagerCompat.notify(NOTIFICACION_ID, builder.build());
    }
}