package us.skyguardian.driverns6;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MyRecyclerAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    //Header header;
    List<ListItem> list;
    public MyRecyclerAdapter(List<ListItem> headerItems) {
        this.list = headerItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_HEADER) {
            View v = inflater.inflate(R.layout.header_layout, parent, false);
            return  new VHHeader(v);
        } else {
            View v = inflater.inflate(R.layout.cards_layout_agenda, parent, false);
            return new VHItem(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final int i = position;
        if (holder instanceof VHHeader) {
            // VHHeader VHheader = (VHHeader)holder;
            Header  currentItem = (Header) list.get(position);
            VHHeader VHheader = (VHHeader)holder;
            VHheader.txtTitle.setText(currentItem.getHeader());
        } else if (holder instanceof VHItem) {
            ContentItem currentItem = (ContentItem) list.get(position);
            VHItem VHitem = (VHItem) holder;
            VHitem.txtNameViaje.setText(currentItem.getIdViaje());
            VHitem.txtNameFecha.setText(currentItem.getFecha());
            VHitem.txtNameOrigen.setText(currentItem.getOrigen());
            VHitem.txtNameDestino.setText(currentItem.getDestino());
            VHitem.txtFechaLlegada.setText(currentItem.getFechaLlegada());
            VHitem.txtTelefono.setText(currentItem.getTelefono());
            VHitem.txtNombreViaje.setText(currentItem.getNombreViaje());
            VHitem.txtLlave.setText(currentItem.getKey());
            VHitem.txtUnidad.setText(currentItem.getUnit());
            VHitem.txtVal.setText(currentItem.getValor());
            VHitem.txtParam.setText(currentItem.getParametros());
            VHitem.txtTelefonoLogin.setText(currentItem.getTelefonoLogin());
            //VHitem.txtNombreUnidad.setText(currentItem.getNombreUnidad());
            VHitem.agenda.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    System.out.println("Ingresó al cardview" + currentItem.getIdViaje());
                    Intent intent = new Intent(view.getContext(), ViajeActivity.class);
                    intent.putExtra("telefonoAgenda", VHitem.txtTelefono.getText().toString());
                    intent.putExtra("nameViajeAgenda", VHitem.txtNombreViaje.getText().toString());
                    intent.putExtra("unidadAgenda", VHitem.txtUnidad.getText().toString());
                    intent.putExtra("llaveAgenda", VHitem.txtLlave.getText().toString());
                    intent.putExtra("valorAgenda", VHitem.txtVal.getText().toString());
                    intent.putExtra("paramAgenda", VHitem.txtParam.getText().toString());
                    intent.putExtra("telAgenda", VHitem.txtTelefonoLogin.getText().toString());
                    //intent.putExtra("nombreUnidad", VHitem.txtNombreUnidad.getText().toString());
                    view.getContext().startActivity(intent);
                }
            });
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return list.get(position) instanceof Header;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

class VHHeader extends RecyclerView.ViewHolder{
    TextView txtTitle;
    public VHHeader(View itemView) {
        super(itemView);
        this.txtTitle = (TextView) itemView.findViewById(R.id.header_id);
    }
}
class VHItem extends RecyclerView.ViewHolder{
    TextView txtNameOrigen;
    TextView txtNameDestino;
    TextView txtNameFecha;
    TextView txtNameViaje;
    TextView txtFechaLlegada;
    TextView txtTelefono;
    TextView txtNombreViaje;
    TextView txtLlave;
    TextView txtUnidad;
    TextView txtParam;
    TextView txtVal;
    TextView txtTelefonoLogin;
    //TextView txtNombreUnidad;
    CardView agenda;

    public VHItem(View itemView) {
        super(itemView);
        this.txtNameFecha = itemView.findViewById(R.id.item_content);
        this.txtNameOrigen =  itemView.findViewById(R.id.item_content2);
        this.txtNameDestino = itemView.findViewById(R.id.item_content5);
        this.txtNameViaje = itemView.findViewById(R.id.item_content4);
        this.txtFechaLlegada = itemView.findViewById(R.id.item_content3);
        this.agenda = itemView.findViewById(R.id.card_view_agenda);
        this.txtTelefono = itemView.findViewById(R.id.item_content6);
        this.txtNombreViaje = itemView.findViewById(R.id.item_content7);
        this.txtLlave = itemView.findViewById(R.id.item_llave);
        this.txtUnidad = itemView.findViewById(R.id.item_unit);
        this.txtParam = itemView.findViewById(R.id.item_param);
        this.txtVal = itemView.findViewById(R.id.item_valida);
        this.txtTelefonoLogin = itemView.findViewById(R.id.item_telefonoLogin);
        //this.txtNombreUnidad = itemView.findViewById(R.id.item_nombreUnidad);
    }
}
