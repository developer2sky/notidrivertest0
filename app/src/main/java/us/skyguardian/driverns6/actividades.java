package us.skyguardian.driverns6;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.gurtam.wiatagkit.Message;
import com.gurtam.wiatagkit.MessageSender;
import com.gurtam.wiatagkit.MessageSenderListener;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class actividades extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private static String receiveParam;

    // Variables para ayuda de BD.
    private SQLiteDatabase db;
    private String numeroviaje = "", nombreunidad = "";
    private String idUnit = "";
    private String auxQuery = "", cuenta, coordenadas, str;
    ArrayList<String> values = new ArrayList<String>();
    StringBuffer sb = new StringBuffer();
    DrawerLayout drawer;


    // Intent llamada y permiso para realizar llamadas desde el movil.
    Intent dial;
    private static final int PHONE_CALL = 102;

    // Puerto e IP para el uso de wiatag.
    String host = "193.193.165.165";
    int port = 20963;
    ProgressDialog progressDialog;
    Integer counter = 1;
    //Inicializa Image Botton
    private ImageButton btnImgChat, mainDrawer;
    private FloatingActionButton btnImgSos, btnImgRoute;
    private BottomNavigationView bottomNavigationView;

    //Variables para el permiso de localización;
    private static final int LOCATION_COARSE = 100;
    private static final int LOCATION_FINE = 101;
    private static final int READ_PHONE_STATE = 107;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationManager locationManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    LocationTrack locationTrack;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();

    protected static final String TAG = "LocationOnOff";

    String[] linkLocation = {""};

    private TextView lblNoViaje;
    private TextView lblHourSalida;
    private TextView lblHourArrive;
    private TextView lblOrigen;
    private TextView lblDestino;
    private TextView lblSenal, lblnumero, lblInstrucciones, lblnombreUnidad;
    private int MY_PERMISSION = 1000;
    private final static int ALL_PERMISSIONS_RESULT = 101;
    double LongData, LatData;

    String val, tel, validaCarga = "", key, pantalla, recibeUnidad, telematics, paramH, userId;
    AlertDialog alert = null;
    String[] MsgTexto = {" En recepción de mercancia.",//1
            " En entrega de mercancia. "  //2
    };

    private com.hzitoun.camera2SecretPictureTaker.services.APictureCapturingService pictureService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_activity_curso);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        navigationView.bringToFront();

        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);

        permissionsToRequest = findUnAskedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0)
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);


        lblNoViaje = findViewById(R.id.xml_nolblViaje);
        lblHourSalida = findViewById(R.id.xml_lblSalidaHour);
        lblHourArrive = findViewById(R.id.xml_lblLlegada);
        lblOrigen = findViewById(R.id.xml_lblOrigen);
        lblDestino = findViewById(R.id.xml_lblDestino);
        lblSenal = findViewById(R.id.xml_lblSenal);
        lblnumero = findViewById(R.id.numEmergencia);
        lblInstrucciones = findViewById(R.id.indSeguridad);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        btnImgRoute = findViewById(R.id.xml_btnImgRoute);
        btnImgSos = findViewById(R.id.xml_btnImgSos);
        swipeRefreshLayout = findViewById(R.id.swipeRefresh);
        lblnombreUnidad = findViewById(R.id.xml_nolblunidad);
        mainDrawer = findViewById(R.id.btnDrawer);

        bottomNavigationView.setSelectedItemId(R.id.curso_bottom);

        val = getIntent().getStringExtra("valida");
        tel = getIntent().getStringExtra("phone");
        key = getIntent().getStringExtra("llave");
        cuenta = getIntent().getStringExtra("cuenta");
        userId = getIntent().getStringExtra("userId");
        pantalla = getIntent().getStringExtra("validacionP");
        //receiveParam = getIntent().getStringExtra("paramH");
        telematics = getIntent().getStringExtra("telematics");
        paramH = getIntent().getStringExtra("paramH");
        System.out.println("validación de pantalla principal:" + pantalla);

        String auxphone = "+52" + tel;
        String auxkey = key;

        int validaInternet = verificarSenal();
        if(validaInternet == 1) {
            new getTripsForPhoneTask().execute(auxphone, auxkey);
        }else{
            if(validaInternet == 0){
                Toast.makeText(actividades.this,"Sin conexión a internet, vualva a intentar.",Toast.LENGTH_LONG).show();
            }
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    new getTripsForPhoneTask().execute(auxphone, auxkey);
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(actividades.this,"Sin conexión a internet, vuelva a intentar",Toast.LENGTH_LONG).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
            }
        });

        mainDrawer.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View view) {
                if(!drawer.isDrawerOpen(Gravity.START)) drawer.openDrawer(Gravity.START);
                else drawer.closeDrawer(Gravity.END);
            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_home:
                        Intent intentD = new Intent(actividades.this, DatosUsuario.class);
                        intentD.putExtra("cuenta", cuenta);
                        intentD.putExtra("userId", userId);
                        startActivity(intentD);
                        break;
                    case R.id.nav_gallery:
                        Intent intentN = new Intent(actividades.this, Normas.class);
                        startActivity(intentN);
                        break;
                    case R.id.nav_slideshow:
                        Intent intentR = new Intent(actividades.this, ReporteBitacora.class);
                        intentR.putExtra("cuenta", cuenta);
                        intentR.putExtra("userId", userId);
                        startActivity(intentR);
                        break;
                    case R.id.nav_hist:
                        Intent intentH = new Intent(actividades.this, Historial.class);
                        intentH.putExtra("cuenta", cuenta);
                        intentH.putExtra("userId", userId);
                        startActivity(intentH);
                        break;
                    case R.id.nav_close:
                        Intent intent = new Intent(actividades.this, LoginActivity.class);
                        Toast.makeText(getApplicationContext(), "Cerrando sesión...", Toast.LENGTH_LONG).show();
                        startActivity(intent);
                        break;
                }
                return false;
            }
        });



        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.agenda_bottom:
                        Intent intent2 = new Intent(actividades.this, AgendaActivity.class);
                        intent2.putExtra("valida", "1");
                        intent2.putExtra("llave", key);
                        intent2.putExtra("unit", recibeUnidad);
                        intent2.putExtra("paramH", paramH);
                        intent2.putExtra("phone", tel);
                        intent2.putExtra("telmatics", telematics);
                        intent2.putExtra("cuenta", cuenta);
                        intent2.putExtra("userId", userId);
                        startActivity(intent2);
                        break;
                    case R.id.bitacora_bottom:
                        Intent intent = new Intent(actividades.this, Bitacora.class);
                        intent.putExtra("valida", "1");
                        intent.putExtra("param", paramH);
                        intent.putExtra("llave", key);
                        intent.putExtra("phone", tel);
                        intent.putExtra("telmatics", telematics);
                        intent.putExtra("cuenta", cuenta);
                        intent.putExtra("userId", userId);
                        startActivity(intent);
                        break;
                    case R.id.curso_bottom:
                        break;
                    case R.id.home_bottom:
                        Intent intent3 = new Intent(actividades.this, HomeActivity.class);
                        intent3.putExtra("valida", "1");
                        intent3.putExtra("param", paramH);
                        intent3.putExtra("llave", key);
                        intent3.putExtra("phone", tel);
                        intent3.putExtra("telmatics", telematics);
                        intent3.putExtra("cuenta", cuenta);
                        intent3.putExtra("userId", userId);
                        startActivity(intent3);
                        break;
                }
                return true;
            }
        });

        //accederPermisoCall();
        //Bundle canalBundle = new Bundle();
        //receiveParam = getIntent().getStringExtra("param");
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        pictureService = com.hzitoun.camera2SecretPictureTaker.services.PictureCapturingServiceImpl.getInstance(this);
        /* Modulo para la obtencion de datos de API REST*/
        //JSONObject jo = new JSONObject(receiveParam);
        String unitId = "";
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE))) {
                //mostrarExplicacionStatePhone();

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE);
            }
        }else{
            //El permiso ya fue activado
        }

        if (telephonyManager.getDeviceId() != null) {
            unitId = telephonyManager.getDeviceId();
            Log.e("IMEI: ", unitId);
        }
        String password = "C0ntr0lCCTSky19";
        MessageSender.initWithHost(host,port,key,password);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        //Valida si gps no esta encendido
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            AlertNoGps();
        }




        btnImgRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(actividades.this, "Mostrando la ruta", Toast.LENGTH_LONG).show();
                displayRoute(str);
            }
        });



        btnImgSos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    locationTrack = new LocationTrack(actividades.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                        //Toast.makeText(getApplicationContext(), "Longitude:" + Double.toString(longitude) + "\nLatitude:" + Double.toString(latitude), Toast.LENGTH_SHORT).show();
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    accederPermisoLocation();
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(actividades.this,"Mensaje guardado, debido a perdida de conexión de Internet.",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    private void displayRoute(String coordenadas){
        try{
            Uri uri = Uri.parse("https://www.google.co.in/maps/dir/" + coordenadas);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            intent.setPackage("com.google.android.apps.maps");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }catch (ActivityNotFoundException e){
            Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.maps");
            Intent intentI = new Intent(Intent.ACTION_VIEW, uri);
            intentI.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intentI);
        }
    }

    private int verificarSenal(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return 1;
        } else {
            return 0;
        }
    }

    private void accederPermisoLocation() {

        //si la API 23 a mas
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Habilitar permisos para la version de API 23 a mas

            int verificarPermisoLocationCoarse = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int verificarPermisoLocationFine = ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
            //Verificamos si el permiso no existe
            if (verificarPermisoLocationCoarse != PackageManager.PERMISSION_GRANTED && verificarPermisoLocationFine != PackageManager.PERMISSION_GRANTED) {
                //verifico si el usuario a rechazado el permiso anteriormente
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION) && shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Si a rechazado el permiso anteriormente muestro un mensaje

                    //mostrarExplicacion();
                } else {
                    //De lo contrario carga la ventana para autorizar el permiso
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                }

            } else {
                //Si el permiso fue activado llamo al metodo de ubicacion
                fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<android.location.Location>() {
                    @Override
                    public void onSuccess(android.location.Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {

                            linkLocation[0] = "http://maps.google.com/maps?q=loc:"+LatData+","+LongData;
                            System.out.println("coordenadas nuevas: " + LatData + ", " + LongData);

                            new registerEvent().execute(idUnit,String.valueOf(LatData),String.valueOf(LongData),"SOS enviado, atenta reacción, favor de marcar o actuar de manera veloz.");

                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                            Message message = new Message()
                                    .time(new Date().getTime())
                                    .location(new com.gurtam.wiatagkit.Location(LatData,LongData,location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                    .Sos()
                                    .batteryLevel((byte)batLevel)
                                    .text("Se ha presionado SOS en la ubicación: " + linkLocation[0] + " con nivel de bateria: " + batLevel + "%");

                            MessageSender.sendMessage(message,new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                        }else{

                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

                            Message message = new Message()
                                    .time(new Date().getTime())
                                    .Sos()
                                    .batteryLevel((byte)batLevel)
                                    .text("Se ha presionado SOS en la ubicación: " + linkLocation[0] + " con nivel de bateria: " + batLevel + "%");

                            MessageSender.sendMessage(message,new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
        } else {//Si la API es menor a 23 - llamo al metodo de ubicacion
            fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<android.location.Location>() {
                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {

                        linkLocation[0] = "http://maps.google.com/maps?q=loc:"+LatData+","+LongData;

                        new registerEvent().execute(idUnit,String.valueOf(LatData),String.valueOf(LongData),"OPERADOR: SOS ENVIADO, ATENTA REACCIÓN, FAVOR DE MARCAR O ACTUAR DE MANERA VELOZ.");

                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = 0;

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        }

                        Message message = new Message()
                                .time(new Date().getTime())
                                .location(new com.gurtam.wiatagkit.Location(LatData,LongData,location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                .Sos()
                                .batteryLevel((byte)batLevel)
                                .text("Se ha presionado SOS en la ubicación: " + linkLocation[0] + " con nivel de bateria: " + batLevel + "%");

                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                    }
                    else{

                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = 0;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        }
                        //new ChatActivity.sendMessageTelegram().execute("Se ha presionado SOS en la ubicación: " + linkLocation[0]);
                        Message message = new Message()
                                .time(new Date().getTime())
                                .Sos()
                                .batteryLevel((byte)batLevel)
                                .text("Se ha presionado SOS en la ubicación: desconocida con nivel de bateria: " + batLevel + "%");

                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
    }

    private void accederPermisoLoc(final String mensaje, final int validaActividad) {

        //si la API 23 a mas
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Habilitar permisos para la version de API 23 a mas

            int verificarPermisoLocationCoarse = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int verificarPermisoLocationFine = ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
            //Verificamos si el permiso no existe
            if (verificarPermisoLocationCoarse != PackageManager.PERMISSION_GRANTED && verificarPermisoLocationFine != PackageManager.PERMISSION_GRANTED) {
                //verifico si el usuario a rechazado el permiso anteriormente
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION) && shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Si a rechazado el permiso anteriormente muestro un mensaje
                    mostrarExplicacion();
                } else {
                    //De lo contrario carga la ventana para autorizar el permiso
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                }

            } else {
                //Si el permiso fue activado llamo al metodo de ubicacion
                fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {

                            linkLocation[0] = "http://maps.google.com/maps?q=loc:"+LatData+","+LongData;
                            //new sendMessageTelegram().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mensaje + linkLocation[0]);

                            new actividades.registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,idUnit,String.valueOf(LatData),String.valueOf(LongData),mensaje);

                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);


                            Message message = new Message()
                                    .time(new Date().getTime())
                                    .location(new com.gurtam.wiatagkit.Location(LatData, LongData, location.getAltitude(), location.getSpeed(), (short) location.getBearing(), (byte)location.getAccuracy()))
                                    .batteryLevel((byte)batLevel)
                                    .text("AVISO: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");
                            System.out.println("mensaje gurtam: " +  message);

                            MessageSender.sendMessage(message, new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Notificación enviada con éxito a monitoreo.\n"+mensaje,Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío de la notificación.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }); //método de envío de mensaje a wialon.

                            if(validaActividad == 3){
                                Intent intent = new Intent (actividades.this, registerFuel.class);
                                intent.putExtra("unit", idUnit);
                                intent.putExtra("x", location.getLongitude());
                                intent.putExtra("y", location.getLatitude());
                                //startActivity
                                startActivity(intent);
                            }
                        }
                        else{
                            new actividades.registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,idUnit,"0","0",mensaje);

                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);


                            Message message = new Message()
                                    .time(new Date().getTime())
                                    //.location(new com.gurtam.wiatagkit.Location(LatData,LongData,location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                    .batteryLevel((byte)batLevel)
                                    .text("AVISO: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");

                            MessageSender.sendMessage(message,new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Notificación enviada con éxito a monitoreo.\n"+mensaje,Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío de la notificación.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                            if(validaActividad == 3) {
                                Intent intent = new Intent(actividades.this, registerFuel.class);
                                intent.putExtra("unit", idUnit);
                                intent.putExtra("x", 0);
                                intent.putExtra("y", 0);
                                startActivity(intent);
                            }
                        }
                    }
                });
            }

        } else {//Si la API es menor a 23 - llamo al metodo de ubicacion
            fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        // Logic to handle location object
                        //System.out.println(.*)
                        linkLocation[0] = "http://maps.google.com/maps?q=loc:"+location.getLatitude()+","+location.getLongitude();
                        new actividades.registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,idUnit,String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()),mensaje);
                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        Message message = new Message()
                                .time(new Date().getTime())
                                .location(new com.gurtam.wiatagkit.Location(LatData, LongData,location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                .batteryLevel((byte)batLevel)
                                .text("AVISO: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");

                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Notificación enviada con éxito a monitoreo.\n"+mensaje,Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío de la notificación.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                    }
                    else{
                        //new sendMessageTelegram().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mensaje + "Ubicación desconocida");
                        new actividades.registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,idUnit,"0","0",mensaje);
                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        //new ChatActivity.sendMessageTelegram().execute("Se ha presionado SOS en la ubicación: " + linkLocation[0]);
                        Message message = new Message()
                                .time(new Date().getTime())
                                //.location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                .batteryLevel((byte)batLevel)
                                .text("AVISO: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");

                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Notificación enviada con éxito a monitoreo.\n"+mensaje,Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío de la notificación.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
    }

    private void mostrarExplicacion() {
        new AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para acceder a la ultima y actual ubicación del dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    public void mensajeAccionCancelada() {
        Toast.makeText(getApplicationContext(), "Haz rechazado la petición, puede suceder que la app no trabaje de manera adecuada.", Toast.LENGTH_SHORT).show();
    }
    public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
        if (picturesTaken != null && !picturesTaken.isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                picturesTaken.forEach((pictureUrl, pictureData) -> {
                    //convert the byte array 'pictureData' to a bitmap (no need to read the file from the external storage) but in case you
                    //You can also use 'pictureUrl' which stores the picture's location on the device
                    final Bitmap bitmap = BitmapFactory.decodeByteArray(pictureData, 0, pictureData.length);
                    Message message = new Message()
                            .time(new Date().getTime())
                            .image("Foto", pictureData)
                            .text("Foto recibida desde CCT por SOS.");

                    MessageSender.sendMessage(message,new MessageSenderListener()
                    {
                        @Override
                        protected void onSuccess() {
                            super.onSuccess();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplication()," Enviando imagen... ",Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                        @Override
                        protected void onFailure(byte errorCode) {
                            super.onFailure(errorCode);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplication(),"Error en el envío de imagen.",Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    });
                });
            }
            //showToast("Done capturing all photos!");
            return;
        }
        //showToast("No camera detected!");
    }


    private void showToast(final String text) {
        runOnUiThread(() ->
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show()
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                drawer.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }


    public class registerEvent extends AsyncTask<String, Void, String>{

        String saveResponse = "";

        @Override
        protected String doInBackground(String... strings) {
            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("unit_id", strings[0]);
                paramObject.put("unit_x", strings[1]);
                paramObject.put("unit_y", strings[2]);
                paramObject.put("unit_txt", strings[3]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/add/event")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            //System.out.println(.*)
            return saveResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //System.out.println(.*)
            try {
                JSONObject jo = new JSONObject(result);
                String success = jo.getString("success");

                if(success != "false"){
                    //Toast.makeText(getApplicationContext(), "Notificación registrada exitosamente", Toast.LENGTH_LONG).show();
                }else{
                    String msg = jo.getString("message");
                    ///Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                Toast.makeText(getBaseContext(),"Error en el servidor, para registro de evento.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    private class getTripsForPhoneTask extends AsyncTask<String, Void, String> {

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            counter = 1;
            progressDialog = new ProgressDialog(actividades.this);
            progressDialog.setMessage("Verificando viajes...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            System.out.println("Params: " + params);
            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("telefono", params[0]);
                paramObject.put("key", params[1]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();
            //ejemplo en la linea 250 /viaje
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/get/phonedriver")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                if(response.code() == 404){
                    saveResponse =  response.body().string().toString();
                }else {
                    if(response.code() == 400){
                        saveResponse =  response.body().string().toString();
                    }else{
                        if(response.isSuccessful()){
                            saveResponse =  response.body().string().toString();
                        }else{
                            saveResponse = response.body().string().toString();
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        protected void onProgressUpdate(Integer... values) {
            //super.onProgressUpdate(values);
            progressDialog.setProgress((int)(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            /*progressBar.setVisibility(View.GONE);
            lblloading.setVisibility(View.GONE);*/

            try {
                if(result.isEmpty()){
                    progressDialog.cancel();
                    String msg = "Sin viajes asignados";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                }else {
                    JSONObject jo = new JSONObject(result);
                    String success = jo.getString("success");
                    if (success != "false") {
                        receiveParam = result;
                        //urlCartaPorte = jo.getString("carta_porte");
                        numeroviaje = jo.getString("id_viaje");
                        idUnit = jo.getString("id_unit");
                        //nombreunidad = jo.getString("nombreunidad");

                        lblNoViaje.setText(numeroviaje);
                        //lblnombreUnidad.setText(nombreunidad);

                        //String auxFechasStr = "SALIDA: ";
                        SpannableString str1 = new SpannableString(jo.getString("hour_exist"));
                        //str1.setSpan(new StyleSpan(Typeface.BOLD), 0, auxFechasStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        lblHourSalida.setText(str1);

                        //String auxFechaStr1 = "LLEGADA: ";
                        SpannableString strA = new SpannableString(jo.getString("hour_arrive"));
                        //strA.setSpan(new StyleSpan(Typeface.BOLD), 0, auxFechaStr1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        lblHourArrive.setText(strA);

                        //String auxOrigenStr = "ORIGEN: ";
                        SpannableString str2 = new SpannableString(jo.getString("origen"));
                        //str2.setSpan(new StyleSpan(Typeface.BOLD), 0, auxOrigenStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        lblOrigen.setText(str2);

                        //String auxDestinoStr = "DESTINO: ";
                        SpannableString str3 = new SpannableString(jo.getString("destino"));
                        //str3.setSpan(new StyleSpan(Typeface.BOLD), 0, auxDestinoStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        lblDestino.setText(str3);

                        SpannableString str4 = new SpannableString(jo.getString("telefonos_emergencia"));
                        lblnumero.setText(str4);

                        SpannableString str5 = new SpannableString(jo.getString("info_seguridad"));
                        lblInstrucciones.setText(str5);

                        progressDialog.cancel();
                        bottomNavigationView.setVisibility(View.VISIBLE);
                        swipeRefreshLayout.setRefreshing(false);

                        String odm = lblNoViaje.getText().toString();
                        new getCoordOdm().execute(odm);

                    } else {
                        progressDialog.cancel();
                        final AlertDialog.Builder builder = new AlertDialog.Builder(actividades.this);
                        builder.setMessage("¡Sin viajes asignados del día de hoy!")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        //bottomNavigationView.setVisibility(View.INVISIBLE);
                                        swipeRefreshLayout.setRefreshing(false);
                                    }
                                });
                        alert = builder.create();
                        alert.show();
                    }
                }
            } catch (JSONException e) {
                progressDialog.cancel();
                Toast.makeText(getApplicationContext(),"Upss, ocurrio un error intentalo de nuevo.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

        }
    }

    private class getCoordOdm extends AsyncTask<String, Void, String> {

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(actividades.this);
            progressDialog.setMessage("Cargando resumen del día ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            JSONObject paramObject = new JSONObject();
            try {
                System.out.println("parametros: " + params);
                System.out.println("cuenta: " + params[0]);
                paramObject.put("numero_viaje",  params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/coordenadasOdm")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        protected void onProgressUpdate(Integer... values) {
            //super.onProgressUpdate(values);
            progressDialog.setProgress((int)(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jo = new JSONObject(result);
                String success = jo.getString("success");

                if(success != "false"){
                    progressDialog.cancel();
                    values.clear();
                    for (int j = 0; j < jo.getJSONArray("inf").length(); j++) {

                        coordenadas = jo.getJSONArray("inf").getJSONArray(j).get(1).toString();
                        values.add(coordenadas);
                        System.out.println("coordenadas: " + coordenadas);
                    }

                    for (String s : values) {
                        sb.append(s);
                        sb.append("/");
                    }
                    str = sb.toString();
                    System.out.println(str);
                }else{
                    progressDialog.cancel();
                    String msg = jo.getString("message");
                    Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                progressDialog.cancel();
                Toast.makeText(getBaseContext(),"Error en .",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }


    private void AlertNoGps(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¡GPS desactivado, favor de activarlo!")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });
        alert = builder.create();
        alert.show();
    }

    @Override
    public void onBackPressed() {
    }


    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                }
                break;
        }

    }
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(actividades.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        locationTrack.stopListener();
    }
}
