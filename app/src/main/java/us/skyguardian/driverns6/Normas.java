package us.skyguardian.driverns6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class Normas extends AppCompatActivity {
    Button button;
    ImageButton back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_normas);

        button = (Button) findViewById(R.id.normaButton);
        back = findViewById(R.id.backNorm);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browseintent=new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.dof.gob.mx/nota_detalle.php?codigo=5485842&fecha=07/06/2017"));
                startActivity(browseintent);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        return;
    }
}