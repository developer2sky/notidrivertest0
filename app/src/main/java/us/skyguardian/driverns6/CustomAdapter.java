package us.skyguardian.driverns6;

import androidx.appcompat.view.menu.ActionMenuItem;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    //Header header;
    List<ListItem> list;
    public CustomAdapter(List<ListItem> headerItems) {
        this.list = headerItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_HEADER) {
            View v = inflater.inflate(R.layout.header_layout, parent, false);
            return  new VHeader(v);
        } else {
            View v = inflater.inflate(R.layout.card_resumen, parent, false);
            return new VItem(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHeader) {
            Header  currentItem = (Header) list.get(position);
            VHeader VHeader = (VHeader)holder;
            VHeader.txtTitle.setText(currentItem.getHeader());
        } else if (holder instanceof VItem) {
            ItemList item = (ItemList) list.get(position);
            VItem VItem = (VItem) holder;
            VItem.txtHoraInicio.setText(item.getHoraInicio());
            VItem.txtDuracion.setText(item.getDuracion());
            VItem.txtUbicacion.setText(item.getUbicacion());
            if(item.getIdEstatus().equals("1")){
                VItem.imageView.setImageResource(R.drawable.conduciendo_bitacora);
            }else if(item.getIdEstatus().equals("2")){
                VItem.imageView.setImageResource(R.drawable.actividades_bitacora);
            }else if(item.getIdEstatus().equals("3")){
                VItem.imageView.setImageResource(R.drawable.pausa_bitacora_icon);
            }else if(item.getIdEstatus().equals("4")){
                VItem.imageView.setImageResource(R.drawable.descanso_notidriver_png);
            }else if(item.getIdEstatus().equals("5")){
                VItem.imageView.setImageResource(R.drawable.caso_bitacora_icon);
            }else if(item.getIdEstatus().equals("6")){
                VItem.imageView.setImageResource(R.drawable.fuera_servicio_icon);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return list.get(position) instanceof Header;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

class VHeader extends RecyclerView.ViewHolder{
    TextView txtTitle;
    public VHeader(View itemView) {
        super(itemView);
        this.txtTitle = (TextView) itemView.findViewById(R.id.header_id);
    }
}
class VItem extends RecyclerView.ViewHolder{
    TextView txtHoraInicio;
    TextView txtDuracion;
    TextView txtUbicacion;
    ImageView imageView;
    CardView notificacion;

    public VItem(View itemView) {
        super(itemView);
        this.txtHoraInicio = (TextView) itemView.findViewById(R.id.horaInicio);
        this.txtDuracion =  (TextView) itemView.findViewById(R.id.duracion);
        this.txtUbicacion = (TextView) itemView.findViewById(R.id.direccion);
        this.imageView = (ImageView) itemView.findViewById(R.id.tipoNot);
        this.notificacion = itemView.findViewById(R.id.card_view_resumen);
    }
}
