package us.skyguardian.driverns6;

import android.text.SpannableString;
import android.text.Spanned;

public class ContentItem extends ListItem {

    private SpannableString idViaje;
    private SpannableString origen;
    private SpannableString destino;
    private SpannableString fecha;
    private SpannableString fechaLlegada;
    private String telefono;
    private String key;
    private String unit;
    private String valor;
    private String parametros;
    private String nombreUnidad;

    public String getNombreUnidad() {
        return nombreUnidad;
    }

    public void setNombreUnidad(String nombreUnidad) {
        this.nombreUnidad = nombreUnidad;
    }

    public String getTelefonoLogin() {
        return telefonoLogin;
    }

    public void setTelefonoLogin(String telefonoLogin) {
        this.telefonoLogin = telefonoLogin;
    }

    private String telefonoLogin;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }

    public String getUnit() {
        return unit;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getNombreViaje() {
        return nombreViaje;
    }

    public void setNombreViaje(String nombreViaje) {
        this.nombreViaje = nombreViaje;
    }

    private String nombreViaje;

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public SpannableString getIdViaje() {
        return idViaje;
    }

    public void setIdViaje(SpannableString idViaje) {
        this.idViaje = idViaje;
    }

    public SpannableString getFechaLlegada() {
        return fechaLlegada;
    }

    public void setFechaLlegada(SpannableString fechaLlegada) {
        this.fechaLlegada = fechaLlegada;
    }

    public SpannableString getOrigen() {
        return origen;
    }

    public void setOrigen(SpannableString origen) {
        this.origen = origen;
    }

    public SpannableString getDestino() {
        return destino;
    }

    public void setDestino(SpannableString destino) {
        this.destino = destino;
    }

    public SpannableString getFecha() {
        return fecha;
    }

    public void setFecha(SpannableString fecha) {
        this.fecha = fecha;
    }
}