package us.skyguardian.driverns6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Button btn_login;
    public static ArrayList<String[]> cuentas;
    boolean permanecer;
    WebView navegador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        /**
         LEER CUENTAS DE USUARIO
         * */
        permanecer = false;

        Bundle extras = getIntent().getExtras();
        if(extras!= null){
            permanecer = extras.getBoolean("PERMANECER");
        }

        SharedPreferences settings = getSharedPreferences("us.skyguardian.skygastelematics", 0);
        cuentas = new ArrayList<String[]>();
        for(int i=1;i<=3;i++){
            if(settings.getString("us.skyguardian.skygastelematics.user"+i, "USER") != "USER"){
                final String[] temp = new String[2];
                temp[0]=settings.getString("us.skyguardian.skygastelematics.user"+i, "USER");
                temp[1]=settings.getString("us.skyguardian.skygastelematics.pass"+i, "PASS");
                cuentas.add(temp);
            }
        }

        if(cuentas.size() > 0 && permanecer == false){
            startActivity(new Intent(MainActivity.this, Cuentas.class));
        }

        btn_login = (Button) findViewById(R.id.btn_login);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,ViajeActivity.class);
                startActivity(i);
            }
        });

    }

}