package us.skyguardian.driverns6;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Bitacora extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private AppBarConfiguration mAppBarConfiguration;
    private ArrayList permissionsToRequest;
    private ProgressDialog progressDialog;
    private BottomNavigationView bottomNavigationView;
    private ArrayList permissionsRejected = new ArrayList();
    private WebView webView;
    private TextView nombre, eco, lic, placa;
    private String android_id = "", key, tel, receiveParam, recibeVal, recibeUnidad, idEstado, hora_inicio, duracion, ubicacion, cuenta, userId, sub, nomOperador, licencia, placas;
    ArrayList<ListItem> arrayList = new ArrayList<ListItem>();
    CustomAdapter adapter;
    RecyclerView recyclerView;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_activity_bitacora);
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        navigationView.bringToFront();

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.main_navigation);


        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();

        tel = getIntent().getStringExtra("phone");
        key = getIntent().getStringExtra("llave");
        receiveParam = getIntent().getStringExtra("param");
        recibeVal = getIntent().getStringExtra("valida");
        cuenta = getIntent().getStringExtra("cuenta");
        userId = getIntent().getStringExtra("userId");

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_bit);
        bottomNavigationView.setSelectedItemId(R.id.bitacora_bottom);
        webView = (WebView) findViewById(R.id.wview);
        recyclerView = findViewById(R.id.recyclerView);
        nombre = (TextView) findViewById(R.id.conductor);
        eco = (TextView) findViewById(R.id.bEco);
        lic = (TextView) findViewById(R.id.b_licencia);
        placa = (TextView) findViewById(R.id.b_placas);


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_home:
                        Intent intentD = new Intent(Bitacora.this, DatosUsuario.class);
                        intentD.putExtra("cuenta", cuenta);
                        intentD.putExtra("userId", userId);
                        startActivity(intentD);
                        break;
                    case R.id.nav_gallery:
                        Intent intentN = new Intent(Bitacora.this, Normas.class);
                        startActivity(intentN);
                        break;
                    case R.id.nav_slideshow:
                        Intent intentR = new Intent(Bitacora.this, ReporteBitacora.class);
                        intentR.putExtra("cuenta", cuenta);
                        intentR.putExtra("userId", userId);
                        startActivity(intentR);
                        break;
                    case R.id.nav_hist:
                        Intent intentH = new Intent(Bitacora.this, Historial.class);
                        intentH.putExtra("cuenta", cuenta);
                        intentH.putExtra("userId", userId);
                        startActivity(intentH);
                        break;
                    case R.id.nav_close:
                        Intent intent = new Intent(Bitacora.this, LoginActivity.class);
                        Toast.makeText(getApplicationContext(), "Cerrando sesión...", Toast.LENGTH_LONG).show();
                        startActivity(intent);
                        break;
                }
                return false;
            }
        });




        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new MyWebViewClient());
        openURL("http://192.169.213.91:9001/apps/cct/stepLineChart.html?rid_telematics="+cuenta+"&useid="+userId);

        new getDataUser().execute(cuenta, userId);
        new getResumeDriver().execute(cuenta, userId);

        /******************************* CAMBIO DE ACTIVIDADES CON EL BOTTOM NAVIGATION VIEW ********************************************************/

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.bitacora_bottom:
                        break;
                    case R.id.home_bottom:
                        Intent intent = new Intent(Bitacora.this, HomeActivity.class);
                            intent.putExtra("valida", "1");
                            intent.putExtra("param", receiveParam);
                            intent.putExtra("llave", key);
                            intent.putExtra("phone", tel);
                            intent.putExtra("cuenta", cuenta);
                            intent.putExtra("userId", userId);
                        startActivity(intent);
                        break;
                    case R.id.agenda_bottom:
                        Intent intent2 = new Intent(Bitacora.this, AgendaActivity.class);
                        intent2.putExtra("valida", "1");
                        intent2.putExtra("llave", key);
                        intent2.putExtra("unit", recibeUnidad);
                        intent2.putExtra("paramH", receiveParam);
                        intent2.putExtra("phone", tel);
                        intent2.putExtra("cuenta", cuenta);
                        intent2.putExtra("userId", userId);
                        startActivity(intent2);
                        break;
                    case R.id.curso_bottom:
                        Intent intent3 = new Intent(Bitacora.this, actividades.class);
                        intent3.putExtra("valida", "1");
                        intent3.putExtra("llave", key);
                        intent3.putExtra("phone", tel);
                        intent3.putExtra("paramH", receiveParam);
                        intent3.putExtra("cuenta", cuenta);
                        intent3.putExtra("userId", userId);
                        startActivity(intent3);
                        break;
                }
                return true;
            }
        });

    }
    private void openURL(String s) {
        webView.loadUrl(s);
        webView.requestFocus();
    }


    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }
                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                }
                break;
        }
    }

    private class getResumeDriver extends AsyncTask<String, Void, String> {

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Bitacora.this);
            progressDialog.setMessage("Cargando resumen del día ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            JSONObject paramObject = new JSONObject();
            try {
                System.out.println("parametros: " + params);
                System.out.println("cuenta: " + params[0] + "," + params[1]);
                paramObject.put("cuenta",  params[0]);
                paramObject.put("id_usuario",  params[1]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/resumenNotificacion")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        protected void onProgressUpdate(Integer... values) {
            //super.onProgressUpdate(values);
            progressDialog.setProgress((int)(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jo = new JSONObject(result);
                String success = jo.getString("success");

                if(success != "false"){
                    progressDialog.cancel();
                        for (int j = 0; j < jo.getJSONArray("inf").length(); j++) {

                            ItemList itemList = new ItemList();

                            idEstado = jo.getJSONArray("inf").getJSONArray(j).get(0).toString();
                            hora_inicio = jo.getJSONArray("inf").getJSONArray(j).get(2).toString();
                            duracion = jo.getJSONArray("inf").getJSONArray(j).get(3).toString();
                            ubicacion = jo.getJSONArray("inf").getJSONArray(j).get(4).toString();

                            /*String[] parts = ubicacion.split(",");
                            String part1 = parts[0];
                            String part2 = parts[1];

                            Double parte1 = Double.parseDouble(part1);
                            Double parte2 = Double.parseDouble(part2);;

                            StringBuilder resultado = new StringBuilder();
                            try {
                                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                                List<Address> addresses = geocoder.getFromLocation(parte1, parte2, 1);
                                if (addresses.size() > 0) {
                                    String address1 = addresses.get(0).getAddressLine(0);
                                    direccion = address1;
                                    //resultado.append(address.getLocality()).append("\n");
                                    //resultado.append(address.getCountryName());
                                    System.out.println("dirección solo coordenadas: " + address1);
                                }
                            } catch (IOException e) {
                                Log.e("tag", e.getMessage());
                            }*/


                            itemList.setIdEstatus(idEstado);
                            itemList.setHoraInicio(hora_inicio);
                            itemList.setDuracion(duracion);
                            itemList.setUbicacion(ubicacion);

                            arrayList.add(itemList);

                            System.out.println("idEstado: " + idEstado + ", hora inicio: " + hora_inicio +  ", duracion: " + duracion + ", ubicacion: " + ubicacion );
                        }

                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Bitacora.this);
                    adapter = new CustomAdapter(arrayList);
                    recyclerView.setLayoutManager(linearLayoutManager);
                    recyclerView.setAdapter(adapter);
                }else{
                    progressDialog.cancel();
                    String msg = jo.getString("message");
                    Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                progressDialog.cancel();
                Toast.makeText(getBaseContext(),"Error en .",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(Bitacora.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private class getDataUser extends AsyncTask<String, Void, String> {

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            JSONObject paramObject = new JSONObject();
            try {
                System.out.println("parametros: " + params);
                System.out.println("cuenta: " + params[0] + "," + params[1]);
                paramObject.put("cuenta",  params[0]);
                paramObject.put("id_usuario",  params[1]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/datosUsuarioNoti")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        protected void onProgressUpdate(Integer... values) {
            //super.onProgressUpdate(values);
            progressDialog.setProgress((int)(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jo = new JSONObject(result);
                String success = jo.getString("success");

                if(success != "false"){
                    progressDialog.cancel();
                    for (int j = 0; j < jo.getJSONArray("inf").length(); j++) {

                        sub = jo.getJSONArray("inf").getJSONArray(j).get(13).toString();
                        placas = jo.getJSONArray("inf").getJSONArray(j).get(15).toString();
                        nomOperador = jo.getJSONArray("inf").getJSONArray(j).get(16).toString();
                        licencia = jo.getJSONArray("inf").getJSONArray(j).get(17).toString();

                        System.out.println("sub: " + sub + ", placas: " + placas + ", nombreOperador " + nomOperador + ", licencia: " + licencia);

                        eco.setText(sub);
                        placa.setText(placas);
                        nombre.setText(nomOperador);
                        lic.setText(licencia);
                    }

                }else{
                    String msg = jo.getString("message");
                    Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                Toast.makeText(getBaseContext(),"Error en .",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                drawer.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}