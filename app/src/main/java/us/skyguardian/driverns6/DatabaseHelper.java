package us.skyguardian.driverns6;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Notificacion.db";
    public static final String TABLE_NAME = "bitacora";
    public static final String COL_1 = "id";
    public static final String COL_2 = "estatus";
    public static final String COL_3 = "descripcion";
    public static final String COL_4 = "tipo";
    public static final String COL_5 = "fecha";
    public static final String COL_6 = "hora_inicio";
    public static final String COL_7 = "hora_final";
    public static final String COL_8 = "duracion";
    public static final String COL_9 = "ubicacion";
    public static final String COL_10 = "rid_telematics";
    public static final String COL_11 = "id_usuario";
    public static final String COL_12 = "acumuladoD";
    public static final String COL_13 = "acumuladoC";
    public static final String COL_14 = "banderaCond";
    public static final String COL_15 = "banderaOnDuty";
    public static final String COL_16 = "validaAuxiliar";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME +"(" + COL_1 + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_2 + " INTEGER NOT NULL, " + COL_3 + " INTEGER NOT NULL, " + COL_4 + " INTEGER NOT NULL, " +
                "" + COL_5 + " TEXT, " + COL_6 + " TEXT, " + COL_7 + " TEXT, " + COL_8 + " TEXT, "  + COL_9 + " TEXT, " + COL_10 + " INTEGER NOT NULL, " + COL_11 + " INTEGER NOT NULL , syncDB INTEGER NOT NULL, Online INTEGER NOT NULL," +
                ""+ COL_12 + " TEXT, " + COL_13 + " TEXT, "+ COL_14 + " TEXT, " + COL_15 + " TEXT, "+ COL_16 + " INTEGER NOT NUll)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(int estatus, int descripcion, int tipo, String fecha, String hora_inicio, String hora_fin, String duracion, String ubicacion,
                              int rid_telematics, int id_usuario, int sync, int online, String acumuladoD, String acumuladoC, String conduciendo, String onDuty, int validaAuxiliar){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, estatus);
        contentValues.put(COL_3, descripcion);
        contentValues.put(COL_4, tipo);
        contentValues.put(COL_5, fecha);
        contentValues.put(COL_6, hora_inicio);
        contentValues.put(COL_7, hora_fin);
        contentValues.put(COL_8, duracion);
        contentValues.put(COL_9, ubicacion);
        contentValues.put(COL_10, rid_telematics);
        contentValues.put(COL_11, id_usuario);
        contentValues.put("syncDB", sync);
        contentValues.put("Online", online);
        contentValues.put(COL_12, acumuladoD);
        contentValues.put(COL_13, acumuladoC);
        contentValues.put(COL_14, conduciendo);
        contentValues.put(COL_15, onDuty);
        contentValues.put(COL_16, validaAuxiliar);
        long result = db.insert(TABLE_NAME, null,  contentValues);
        if(result == -1){
            return false;
        }else{
            return true;
        }
    }

    public Cursor getAllData(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME + " where SyncDB = 0", null);
        return  res;
    }

    public Cursor getDataOffline(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor offline = db.rawQuery("select * from " + TABLE_NAME + " where Online = 0", null);
        return offline;
    }

    public boolean updateData(int id, int online){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1, id);
        contentValues.put("Online", online);
        db.update(TABLE_NAME, contentValues, "id = ?", new String[]{String.valueOf(id)});
        return true;
    }

    public Integer deleteData(){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, null, null);
    }

    public boolean updateDataSync(int id, int syncDB){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1, id);
        contentValues.put("syncDB", syncDB);
        db.update(TABLE_NAME, contentValues, "id = ?", new String[]{String.valueOf(id)});
        return true;
    }

    public boolean updateAcumulado(int id, String descanso, String conduciendo, String duracion){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1, id);
        contentValues.put(COL_12, descanso);
        contentValues.put(COL_13, conduciendo);
        contentValues.put(COL_8, duracion);
        db.update(TABLE_NAME, contentValues, "id = ?", new String[]{String.valueOf(id)});
        return true;
    }



    public Cursor ultimoBitacora(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor ultimo = db.rawQuery("SELECT id, hora_inicio, fecha  FROM " + TABLE_NAME + " WHERE descripcion != 16 ORDER BY  id DESC LIMIT 1", null);
        return  ultimo;
    }

    public Cursor penultimoBitacora(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor penultimo = db.rawQuery("SELECT id, hora_inicio, fecha FROM " + TABLE_NAME + " WHERE descripcion != 16 ORDER BY id DESC LIMIT 1, 1", null);
        return  penultimo;
    }


    public Cursor Acumulado(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor acumulado = db.rawQuery("SELECT acumuladoD, acumuladoC FROM " + TABLE_NAME + " ORDER BY  id DESC LIMIT 1", null);
        return  acumulado;
    }

    public Cursor banderaConducirOnDuty(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor bandera = db.rawQuery("SELECT banderaCond, banderaOnDuty FROM " + TABLE_NAME + " ORDER BY  id DESC LIMIT 1", null);
        return  bandera;
    }

    public Cursor validaAux(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor bandera = db.rawQuery("SELECT validaAuxiliar FROM " + TABLE_NAME + " ORDER BY  id DESC LIMIT 1", null);
        return  bandera;
    }


}
