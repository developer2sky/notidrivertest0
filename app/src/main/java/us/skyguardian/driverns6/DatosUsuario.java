package us.skyguardian.driverns6;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.ProgressDialog;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DatosUsuario extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private String cuenta, userId, razon_social, calle, noExt, noInt, colonia,localidad, municipio,estado, pais, cp, tipo_servicio, modalidad, marca, sub, modelo, placas, nomOperador, licencia, vencimiento, correo;
    private TextView razonTx, calleTx, noExtTx, noIntTx, coloniaTx, localidadTx, municipioTx, estadoTx, paisTx, cpTx, tipo_servicioTx, modalidadTx, marcaTx, subTx, modeloTx, placasTx, nomOperadorTx, licenciaTx, vencimientoTx, correoTx;
    ImageButton back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_usuario);

        cuenta = getIntent().getStringExtra("cuenta");
        userId = getIntent().getStringExtra("userId");
        razonTx = findViewById(R.id.razonSocial);
        calleTx = findViewById(R.id.calle);
        noExtTx = findViewById(R.id.noExt);
        noIntTx = findViewById(R.id.noInt);
        coloniaTx = findViewById(R.id.colonia);
        localidadTx = findViewById(R.id.localidad);
        municipioTx = findViewById(R.id.municipio);
        estadoTx = findViewById(R.id.estado);
        paisTx = findViewById(R.id.pais);
        cpTx = findViewById(R.id.cp);
        tipo_servicioTx = findViewById(R.id.tipoServ);
        modalidadTx = findViewById(R.id.modalidad);
        marcaTx = findViewById(R.id.marca);
        subTx = findViewById(R.id.sub);
        modeloTx = findViewById(R.id.modelo);
        placasTx = findViewById(R.id.placas);
        nomOperadorTx = findViewById(R.id.conductor);
        licenciaTx = findViewById(R.id.licencia);
        vencimientoTx = findViewById(R.id.vencim);
        correoTx = findViewById(R.id.email);
        back = findViewById(R.id.backData);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        new getDataUser().execute(cuenta, userId);
    }


    private class getDataUser extends AsyncTask<String, Void, String> {

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(DatosUsuario.this);
            progressDialog.setMessage("Cargando reporte del día ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            JSONObject paramObject = new JSONObject();
            try {
                System.out.println("parametros: " + params);
                System.out.println("cuenta: " + params[0] + "," + params[1]);
                paramObject.put("cuenta",  params[0]);
                paramObject.put("id_usuario",  params[1]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/datosUsuarioNoti")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        protected void onProgressUpdate(Integer... values) {
            //super.onProgressUpdate(values);
            progressDialog.setProgress((int)(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jo = new JSONObject(result);
                String success = jo.getString("success");

                if(success != "false"){
                    progressDialog.cancel();
                    for (int j = 0; j < jo.getJSONArray("inf").length(); j++) {

                        razon_social = jo.getJSONArray("inf").getJSONArray(j).get(0).toString();
                        calle = jo.getJSONArray("inf").getJSONArray(j).get(1).toString();
                        noExt = jo.getJSONArray("inf").getJSONArray(j).get(2).toString();
                        noInt = jo.getJSONArray("inf").getJSONArray(j).get(3).toString();
                        colonia = jo.getJSONArray("inf").getJSONArray(j).get(4).toString();
                        localidad = jo.getJSONArray("inf").getJSONArray(j).get(5).toString();
                        municipio = jo.getJSONArray("inf").getJSONArray(j).get(6).toString();
                        estado = jo.getJSONArray("inf").getJSONArray(j).get(7).toString();
                        pais= jo.getJSONArray("inf").getJSONArray(j).get(8).toString();
                        cp= jo.getJSONArray("inf").getJSONArray(j).get(9).toString();
                        tipo_servicio= jo.getJSONArray("inf").getJSONArray(j).get(10).toString();
                        modalidad= jo.getJSONArray("inf").getJSONArray(j).get(11).toString();
                        marca = jo.getJSONArray("inf").getJSONArray(j).get(12).toString();
                        sub = jo.getJSONArray("inf").getJSONArray(j).get(13).toString();
                        modelo = jo.getJSONArray("inf").getJSONArray(j).get(14).toString();
                        placas = jo.getJSONArray("inf").getJSONArray(j).get(15).toString();
                        nomOperador = jo.getJSONArray("inf").getJSONArray(j).get(16).toString();
                        licencia = jo.getJSONArray("inf").getJSONArray(j).get(17).toString();
                        vencimiento = jo.getJSONArray("inf").getJSONArray(j).get(18).toString();
                        correo = jo.getJSONArray("inf").getJSONArray(j).get(19).toString();


                        System.out.println("razon social: " + razon_social + ", calle: " + calle +  ", No_ext: " + noExt + "No_int: " + noInt +", colonia: " + colonia +
                                ", localidad: " + localidad + ", municipio: " + municipio + ", estado: " + estado + ", pais: " + pais + ", cp: " + cp +
                                ", tipo servicio: " + tipo_servicio + ", modalidad: " + modalidad + ", marca: " + marca + ", sub: " + sub + ", modelo: " + modelo +
                                ", placas: " + placas + ", nombreOperador " + nomOperador + ", licencia: " + licencia + ", vencimiento: " + vencimiento + ", correo: " + correo);

                        razonTx.setText(razon_social);
                        calleTx.setText(calle);
                        noExtTx.setText(noExt);
                        noIntTx.setText(noInt);
                        coloniaTx.setText(colonia);
                        localidadTx.setText(localidad);
                        municipioTx.setText(municipio);
                        estadoTx.setText(estado);
                        paisTx.setText(pais);
                        cpTx.setText(cp);
                        tipo_servicioTx.setText(tipo_servicio);
                        modalidadTx.setText(modalidad);
                        marcaTx.setText(marca);
                        subTx.setText(sub);
                        modeloTx.setText(modelo);
                        placasTx.setText(placas);
                        nomOperadorTx.setText(nomOperador);
                        licenciaTx.setText(licencia);
                        vencimientoTx.setText(vencimiento);
                        correoTx.setText(correo);
                    }

                }else{
                    progressDialog.cancel();
                    String msg = jo.getString("message");
                    Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                progressDialog.cancel();
                Toast.makeText(getBaseContext(),"Error en .",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        return;
    }
}