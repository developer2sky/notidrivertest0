package us.skyguardian.driverns6;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ReporteAdapter  extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    //Header header;
    List<ListItem> list;
    public ReporteAdapter(List<ListItem> headerItems) {
        this.list = headerItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_HEADER) {
            View v = inflater.inflate(R.layout.header_layout, parent, false);
            return  new VHeaderR(v);
        } else {
            View v = inflater.inflate(R.layout.card_reporte, parent, false);
            return new VItemR(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHeaderR) {
            Header  currentItem = (Header) list.get(position);
            VHeaderR VHeaderR = (VHeaderR)holder;
            VHeaderR.txtTitle.setText(currentItem.getHeader());
        } else if (holder instanceof VItemR) {
            itemReporte item = (itemReporte) list.get(position);
            VItemR VItemR = (VItemR) holder;
            VItemR.txtEstatus.setText(item.getEstatus());
            VItemR.txtHora.setText(item.getHora());
            VItemR.txtDuracion.setText(item.getDuracion());
            VItemR.txtDesc.setText(item.getDescripcion());
            VItemR.txtUbicacion.setText(item.getUbicacion());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return list.get(position) instanceof Header;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

class VHeaderR extends RecyclerView.ViewHolder{
    TextView txtTitle;
    public VHeaderR(View itemView) {
        super(itemView);
        this.txtTitle = (TextView) itemView.findViewById(R.id.header_id);
    }
}
class VItemR extends RecyclerView.ViewHolder{
    TextView txtEstatus;
    TextView txtHora;
    TextView txtDuracion;
    TextView txtDesc;
    TextView txtUbicacion;
    CardView notificacion;

    public VItemR(View itemView) {
        super(itemView);
        this.txtEstatus = (TextView) itemView.findViewById(R.id.estatusR);
        this.txtHora = (TextView) itemView.findViewById(R.id.horaR);
        this.txtDuracion =  (TextView) itemView.findViewById(R.id.duracionR);
        this.txtUbicacion = (TextView) itemView.findViewById(R.id.direccionR);
        this.txtDesc =  (TextView) itemView.findViewById(R.id.descR);
        this.notificacion = itemView.findViewById(R.id.card_view_reporte);
    }
}
