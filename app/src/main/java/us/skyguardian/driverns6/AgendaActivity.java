package us.skyguardian.driverns6;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.gurtam.wiatagkit.Message;
import com.gurtam.wiatagkit.MessageSender;
import com.gurtam.wiatagkit.MessageSenderListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AgendaActivity extends AppCompatActivity{

    RecyclerView recyclerView;
    MyRecyclerAdapter adapter;
    private ProgressDialog progressDialog;
    private BottomNavigationView bottomNavigationView;
    private FloatingActionButton btnImgSos;
    ImageButton drawerMain;
    private static String receiveParam, cuenta;
    private TextView origen, destino;
    private com.hzitoun.camera2SecretPictureTaker.services.APictureCapturingService pictureService;
    ArrayList<ListItem> arrayList = new ArrayList<>();
    ListView list;
    String host = "193.193.165.165"; //host y puerto de wialon
    int port = 20963;
    DrawerLayout drawer;

    //Variables para el permiso de localización;
    private static final int LOCATION_COARSE = 100;
    private static final int LOCATION_FINE = 101;
    private static final int READ_PHONE_STATE = 107;
    private FusedLocationProviderClient fusedLocationClient;

    String[] linkLocation = {""};

    String recibeVal, datoLogin, recibeParam, recibeUnit, phoneVal, nombreViaje, key, nombreUnidad, userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_activity_agenda);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        navigationView.bringToFront();

        origen = findViewById(R.id.lbl_origen);
        destino = findViewById(R.id.lbl_destino);
        recyclerView = (RecyclerView)findViewById(R.id.add_header);
        btnImgSos = findViewById(R.id.xml_btnImgSos);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_agenda);
        bottomNavigationView.setSelectedItemId(R.id.agenda_bottom);
        drawerMain = findViewById(R.id.btnDrawerA);
        //Se reciben parámetros de otras activities
        recibeVal = getIntent().getStringExtra("valida");
        recibeUnit = getIntent().getStringExtra("unit");
        receiveParam = getIntent().getStringExtra("paramH");
        phoneVal = getIntent().getStringExtra("phone");
        key = getIntent().getStringExtra("llave");
        cuenta =  getIntent().getStringExtra("cuenta");
        userId =  getIntent().getStringExtra("userId");

        System.out.println("telefono antes de agenda: " + phoneVal + ", " + key);


        String password = "C0ntr0lCCTSky19";
        MessageSender.initWithHost(host,port,key,password);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_home:
                        Intent intentD = new Intent(AgendaActivity.this, DatosUsuario.class);
                        intentD.putExtra("cuenta", cuenta);
                        intentD.putExtra("userId", userId);
                        startActivity(intentD);
                        break;
                    case R.id.nav_gallery:
                        Intent intentN = new Intent(AgendaActivity.this, Normas.class);
                        startActivity(intentN);
                        break;
                    case R.id.nav_slideshow:
                        Intent intentR = new Intent(AgendaActivity.this, ReporteBitacora.class);
                        intentR.putExtra("cuenta", cuenta);
                        intentR.putExtra("userId", userId);
                        startActivity(intentR);
                        break;
                    case R.id.nav_hist:
                        Intent intentH = new Intent(AgendaActivity.this, Historial.class);
                        intentH.putExtra("cuenta", cuenta);
                        intentH.putExtra("userId", userId);
                        startActivity(intentH);
                        break;
                    case R.id.nav_close:
                        Intent intent = new Intent(AgendaActivity.this, LoginActivity.class);
                        Toast.makeText(getApplicationContext(), "Cerrando sesión...", Toast.LENGTH_LONG).show();
                        startActivity(intent);
                        break;
                }
                return false;
            }
        });

        drawerMain.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View view) {
                if(!drawer.isDrawerOpen(Gravity.START)) drawer.openDrawer(Gravity.START);
                else drawer.closeDrawer(Gravity.END);
            }
        });

        btnImgSos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    accederPermisoLocation();
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(AgendaActivity.this,"Mensaje guardado, debido a perdida de conexión de Internet.",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        new getAgendaDriver().execute(phoneVal, "1");


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.agenda_bottom:
                        break;
                    case R.id.home_bottom:
                        Intent intent = new Intent(AgendaActivity.this, HomeActivity.class);
                        intent.putExtra("valida", "1");
                        intent.putExtra("param", receiveParam);
                        intent.putExtra("llave", key);
                        intent.putExtra("phone", phoneVal);
                        intent.putExtra("cuenta", cuenta);
                        intent.putExtra("userId", userId);
                        startActivity(intent);
                        break;
                    case R.id.curso_bottom:
                        Intent intent3 = new Intent(AgendaActivity.this, actividades.class);
                        intent3.putExtra("valida", "1");
                        intent3.putExtra("llave", key);
                        intent3.putExtra("phone", phoneVal);
                        intent3.putExtra("paramH", receiveParam);
                        intent3.putExtra("cuenta", cuenta);
                        intent3.putExtra("userId", userId);
                        startActivity(intent3);
                        break;
                    case R.id.bitacora_bottom:
                        Intent intent2 = new Intent(AgendaActivity.this, Bitacora.class);
                            intent2.putExtra("valida", "1");
                            intent2.putExtra("param", receiveParam);
                            intent2.putExtra("llave", key);
                            intent2.putExtra("phone", phoneVal);
                            intent2.putExtra("cuenta", cuenta);
                            intent2.putExtra("userId", userId);
                        startActivity(intent2);
                        break;

                }
                return true;
            }
        });
    }

    private class getAgendaDriver extends AsyncTask<String, Void, String> {

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AgendaActivity.this);
            progressDialog.setMessage("Cargando agenda...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            JSONObject paramObject = new JSONObject();
            try {
                    System.out.println("parametros: " + params);
                    System.out.println("telefono: " + "+52" +params[0]);
                    System.out.println("data: " + params[1]);
                    paramObject.put("telefono", "+52" + params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/agenda")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        protected void onProgressUpdate(Integer... values) {
            progressDialog.setProgress((int)(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date date = new Date();
            String fechaHoy = dateFormat.format(date);
            String fechaManana = dateFormat.format(sumarRestarDiasFecha(date, 1));
            String fechaManana1 = dateFormat.format(sumarRestarDiasFecha(date, 2));

            try {
                JSONObject jo = new JSONObject(result);
                String success = jo.getString("success");

                if(success != "false"){
                    progressDialog.cancel();
                    //for(int i=0; i < jo.getJSONArray("inf").length(); i++){
                        if(fechaHoy.equals(jo.getJSONArray("inf").getJSONArray(0).get(0)) || fechaManana.equals(jo.getJSONArray("inf").getJSONArray(0).get(0)) || fechaManana1.equals(jo.getJSONArray("inf").getJSONArray(0).get(0)) ) {
                            //System.out.println(jo.getJSONArray("inf").getJSONArray(i).get(0));
                            Header header = new Header();
                            //header.setHeader("Fecha de viaje: " + jo.getJSONArray("inf").getJSONArray(i).get(0).toString());
                            arrayList.add(header);
                            for (int j = 0; j < jo.getJSONArray("inf").length(); j++) {
                                //System.out.println(jo.getJSONArray("inf").getJSONArray(j).getString(1));
                                ContentItem item = new ContentItem();
                                String auxstr = "Salida: ";
                                String auxstr2 = "Origen: ";
                                String auxstr3 = "Destino: ";
                                String auxstr4 = "No. viaje: ";
                                String auxstr5 = "Llegada: ";


                                SpannableString str4 = new SpannableString(auxstr4 + jo.getJSONArray("inf").getJSONArray(j).get(5).toString());
                                str4.setSpan(new StyleSpan(Typeface.BOLD), 0, auxstr4.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                str4.setSpan(new ForegroundColorSpan(Color.parseColor("#009EE2")), 0, auxstr4.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                item.setIdViaje(str4);

                                nombreViaje = jo.getJSONArray("inf").getJSONArray(j).get(5).toString();

                                SpannableString str = new SpannableString(auxstr + jo.getJSONArray("inf").getJSONArray(j).get(3).toString());
                                str.setSpan(new StyleSpan(Typeface.BOLD), 0, auxstr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                str.setSpan(new ForegroundColorSpan(Color.BLACK), 0, auxstr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                item.setFecha(str);

                                SpannableString str2 = new SpannableString(auxstr2 + jo.getJSONArray("inf").getJSONArray(j).get(1).toString());
                                str2.setSpan(new StyleSpan(Typeface.BOLD), 0, auxstr2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                str2.setSpan(new ForegroundColorSpan(Color.BLACK), 0, auxstr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                item.setOrigen(str2);

                                SpannableString str3 = new SpannableString(auxstr3 + jo.getJSONArray("inf").getJSONArray(j).get(2).toString());
                                str3.setSpan(new StyleSpan(Typeface.BOLD), 0, auxstr3.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                str3.setSpan(new ForegroundColorSpan(Color.BLACK), 0, auxstr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                item.setDestino(str3);

                                SpannableString str5 = new SpannableString(auxstr5 + jo.getJSONArray("inf").getJSONArray(j).get(4).toString());
                                str5.setSpan(new StyleSpan(Typeface.BOLD), 0, auxstr5.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                str5.setSpan(new ForegroundColorSpan(Color.BLACK), 0, auxstr5.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                item.setFechaLlegada(str5);

                                item.setNombreViaje(nombreViaje);
                                item.setTelefono("+52" + phoneVal);
                                item.setTelefonoLogin(phoneVal);
                                item.setUnit(recibeUnit);
                                item.setKey(key);
                                item.setValor(recibeVal);
                                arrayList.add(item);
                            }
                        }else{
                            Header header = new Header();
                            header.setHeader("No hay viajes proximos...");
                            arrayList.add(header);

                            AlertDialog alertDialog = new AlertDialog.Builder(AgendaActivity.this).create();
                            alertDialog.setTitle("Sin Viajes...");
                            alertDialog.setMessage("No existen viajes proximos en la agenda.");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        }
                    //}

                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AgendaActivity.this);
                    adapter = new MyRecyclerAdapter(arrayList);
                    recyclerView.setLayoutManager(linearLayoutManager);
                    recyclerView.setAdapter(adapter);

                }else{
                    progressDialog.cancel();
                    String msg = jo.getString("message");
                    Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                }

            } catch (JSONException e) {
                progressDialog.cancel();
                Toast.makeText(getBaseContext(),"Error en número celular o llave móvil, verifique e intentelo de nuevo.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    public Date sumarRestarDiasFecha(Date fecha, int dias){

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(fecha); // Configuramos la fecha que se recibe

        calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de días a añadir, o restar en caso de días<0

        return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos

    }

    private int verificarSenal(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return 1;
        } else {
            return 0;
        }
    }

    private void accederPermisoLocation() {

        //si la API 23 a mas
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Habilitar permisos para la version de API 23 a mas

            int verificarPermisoLocationCoarse = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int verificarPermisoLocationFine = ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
            //Verificamos si el permiso no existe
            if (verificarPermisoLocationCoarse != PackageManager.PERMISSION_GRANTED && verificarPermisoLocationFine != PackageManager.PERMISSION_GRANTED) {
                //verifico si el usuario a rechazado el permiso anteriormente
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION) && shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Si a rechazado el permiso anteriormente muestro un mensaje

                    //mostrarExplicacion();
                } else {
                    //De lo contrario carga la ventana para autorizar el permiso
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                }

            } else {
                //Si el permiso fue activado llamo al metodo de ubicacion
                fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<android.location.Location>() {
                    @Override
                    public void onSuccess(android.location.Location location) {
                        // Got last known location. In some rare situations this can be null.

                        if (location != null) {

                            linkLocation[0] = "http://maps.google.com/maps?q=loc:"+location.getLatitude()+","+location.getLongitude();

                            new registerEvent().execute(recibeUnit,String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()),"SOS enviado, atenta reacción, favor de marcar o actuar de manera veloz.");

                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                            Message message = new Message()
                                    .time(new Date().getTime())
                                    .location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                    .Sos()
                                    .batteryLevel((byte)batLevel)
                                    .text("Se ha presionado SOS en la ubicación: " + linkLocation[0] + " con nivel de bateria: " + batLevel + "%");

                            MessageSender.sendMessage(message,new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                        }else{

                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

                            Message message = new Message()
                                    .time(new Date().getTime())
                                    .Sos()
                                    .batteryLevel((byte)batLevel)
                                    .text("Se ha presionado SOS con nivel de bateria: " + batLevel + "%");

                            MessageSender.sendMessage(message,new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
        } else {//Si la API es menor a 23 - llamo al metodo de ubicacion
            fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<android.location.Location>() {
                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {

                        linkLocation[0] = "http://maps.google.com/maps?q=loc:"+location.getLatitude()+","+location.getLongitude();

                        new registerEvent().execute(recibeUnit,String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()),"OPERADOR: SOS ENVIADO, ATENTA REACCIÓN, FAVOR DE MARCAR O ACTUAR DE MANERA VELOZ.");

                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = 0;

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        }

                        Message message = new Message()
                                .time(new Date().getTime())
                                .location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                .Sos()
                                .batteryLevel((byte)batLevel)
                                .text("Se ha presionado SOS en la ubicación: " + linkLocation[0] + " con nivel de bateria: " + batLevel + "%");

                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                    }
                    else{

                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = 0;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        }
                        //new ChatActivity.sendMessageTelegram().execute("Se ha presionado SOS en la ubicación: " + linkLocation[0]);
                        Message message = new Message()
                                .time(new Date().getTime())
                                .Sos()
                                .batteryLevel((byte)batLevel)
                                .text("Se ha presionado SOS en la ubicación: desconocida con nivel de bateria: " + batLevel + "%");

                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
    }

    public class registerEvent extends AsyncTask<String, Void, String>{

        String saveResponse = "";

        @Override
        protected String doInBackground(String... strings) {
            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("unit_id", strings[0]);
                paramObject.put("unit_x", strings[1]);
                paramObject.put("unit_y", strings[2]);
                paramObject.put("unit_txt", strings[3]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/add/event")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            //System.out.println(.*)
            return saveResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //System.out.println(.*)
            try {
                JSONObject jo = new JSONObject(result);
                String success = jo.getString("success");

                if(success != "false"){
                    Toast.makeText(getApplicationContext(), "Notificación registrada exitosamente", Toast.LENGTH_LONG).show();
                }else{
                    String msg = jo.getString("message");
                    //Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                }


            } catch (JSONException e) {
                Toast.makeText(getBaseContext(),"Error en el servidor, para registro de evento.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
    }
}
