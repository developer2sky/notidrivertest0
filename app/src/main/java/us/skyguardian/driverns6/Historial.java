package us.skyguardian.driverns6;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Historial extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;
    private ProgressDialog progressDialog;
    private String fecha, idNotificacion, duracion, descripcion, cuenta, userId;
    ArrayList<ItemModel> itemsList = new ArrayList<>();
    ImageButton back;
    ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);

        list = (ListView) findViewById(R.id.listview);
        back = findViewById(R.id.backHist);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        cuenta = getIntent().getStringExtra("cuenta");
        userId = getIntent().getStringExtra("userId");
        new getResumeLast().execute(cuenta, userId);

    }

    private ArrayList sortAndAddSections(ArrayList<ItemModel> itemList)
    {

        ArrayList<ItemModel> tempList = new ArrayList<>();
        //First we sort the array
        Collections.sort(itemList);

        //Loops thorugh the list and add a section before each sectioncell start
        String header = "";
        for(int i = 0; i < itemList.size(); i++)
        {
            //If it is the start of a new section we create a new listcell and add it to our array
            if(!(header.equals(itemList.get(i).getDate()))) {
                ItemModel sectionCell = new ItemModel(null, null,null, itemList.get(i).getDate());
                sectionCell.setToSectionHeader();
                tempList.add(sectionCell);
                header = itemList.get(i).getDate();
            }
            tempList.add(itemList.get(i));
        }

        return tempList;
    }


    public class ListAdapter extends ArrayAdapter {

        LayoutInflater inflater;
        public ListAdapter(Context context, ArrayList items) {
            super(context, 0, items);
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            ItemModel cell = (ItemModel) getItem(position);

            //If the cell is a section header we inflate the header layout
            if(cell.isSectionHeader()) {
                v = inflater.inflate(R.layout.section_header, null);

                v.setClickable(false);

                String dateParse = "" + cell.getDate();
                String a = dateParse.substring(0,4);
                String m = dateParse.substring(5,7);
                String d = dateParse.substring(8,10);

                System.out.println("fecha: " + a + "/" + m + "/" + d);

                int x = Integer.parseInt(a);
                int y = Integer.parseInt(m);
                int z  = Integer.parseInt(d);

                System.out.println("fecha2: " + x + "/" + y + "/" + z);

                DateTimeFormatter dateFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);

                LocalDate dateOfBirth = LocalDate.of(x, y, z);
                String formattedDob = dateOfBirth.format(dateFormatter);
                System.out.println("Born: " + formattedDob);

                TextView header = (TextView) v.findViewById(R.id.section_header);
                header.setText(formattedDob);
            }else
            {
                v = inflater.inflate(R.layout.row_item, null);
                TextView time_time = (TextView) v.findViewById(R.id.time_time);
                TextView tv_item_sysdia = (TextView) v.findViewById(R.id.tv_item_sysdia);
                TextView tv_item_plus = (TextView) v.findViewById(R.id.tv_item_plus);
                ImageView image = (ImageView) v.findViewById(R.id.tipoNotH);

                time_time.setText(cell.getItemTime());
                tv_item_sysdia.setText(cell.getItemSysDia());
                tv_item_plus.setText(cell.getItemPulse());

                if(time_time.getText().toString().equals("1")){
                    image.setImageResource(R.drawable.conduciendo_bitacora);
                }else if(time_time.getText().toString().equals("2")){
                    image.setImageResource(R.drawable.actividades_bitacora);
                }else if(time_time.getText().toString().equals("3")){
                    image.setImageResource(R.drawable.pausa_bitacora_icon);
                }else if(time_time.getText().toString().equals("4")){
                    image.setImageResource(R.drawable.descanso_notidriver_png);
                }else if(time_time.getText().toString().equals("5")){
                    image.setImageResource(R.drawable.caso_bitacora_icon);
                }else if(time_time.getText().toString().equals("6")){
                    image.setImageResource(R.drawable.fuera_servicio_icon);
                }


            }
            return v;
        }
    }

    private class getResumeLast extends AsyncTask<String, Void, String> {

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Historial.this);
            progressDialog.setMessage("Cargando resumen del día ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            JSONObject paramObject = new JSONObject();
            try {
                System.out.println("parametros: " + params);
                System.out.println("cuenta: " + params[0]);
                paramObject.put("cuenta",  params[0]);
                paramObject.put("id_usuario",  params[1]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/resumenNotificacionLast")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        protected void onProgressUpdate(Integer... values) {
            //super.onProgressUpdate(values);
            progressDialog.setProgress((int)(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jo = new JSONObject(result);
                String success = jo.getString("success");

                if(success != "false"){
                    progressDialog.cancel();
                    for (int j = 0; j < jo.getJSONArray("inf").length(); j++) {

                        fecha = jo.getJSONArray("inf").getJSONArray(j).get(0).toString();
                        idNotificacion = jo.getJSONArray("inf").getJSONArray(j).get(1).toString();
                        descripcion = jo.getJSONArray("inf").getJSONArray(j).get(2).toString();
                        duracion = jo.getJSONArray("inf").getJSONArray(j).get(3).toString();

                        String subFecha = fecha.substring(0,10);

                        itemsList.add(new ItemModel(idNotificacion, descripcion , duracion, subFecha));
                        System.out.println("fecha: " + subFecha + ", id: " + idNotificacion +  ", descripcion: " + descripcion + ", duracion: " + duracion );
                    }
                    itemsList = sortAndAddSections(itemsList);
                    ListAdapter adapter = new ListAdapter(Historial.this, itemsList);
                    list.setAdapter(adapter);

                }else{
                    progressDialog.cancel();
                    String msg = jo.getString("message");
                    Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                progressDialog.cancel();
                Toast.makeText(getBaseContext(),"Error en .",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        return;
    }
}