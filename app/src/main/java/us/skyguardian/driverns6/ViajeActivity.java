package us.skyguardian.driverns6;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.SpannableString;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.gurtam.wiatagkit.Message;
import com.gurtam.wiatagkit.MessageSender;
import com.gurtam.wiatagkit.MessageSenderListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ViajeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    String recibePhone, recibeName, recibeVal, recibeParam, recibeUnit, key, recibePhoneLogin, nombreUnidad, pantalla,  str, coordenadas;
    private TextView lblNoViaje;
    private TextView lblHourSalida;
    private TextView lblHourArrive;
    private TextView lblOrigen;
    private TextView lblDestino;
    private TextView lbltelLogin;
    private TextView lblnumero, lblInstrucciones, numeroViaje, phoneAgenda, llaveA, unidadA, paramA, valorA, nomUnidad;
    private FloatingActionButton btnImgSos, btnImgRoute;
    ArrayList<String> values = new ArrayList<String>();
    StringBuffer sb = new StringBuffer();
    String host = "193.193.165.165"; //host y puerto de wialon
    int port = 20963;
    ImageButton back;
    //Variables para el permiso de localización;
    private static final int LOCATION_COARSE = 100;
    private static final int LOCATION_FINE = 101;
    private static final int READ_PHONE_STATE = 107;
    private FusedLocationProviderClient fusedLocationClient;
    String[] linkLocation = {""};


    ProgressDialog progressDialog;
    Integer counter = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viaje);

        //Se reciben parámetros de otras activities
        recibePhone = getIntent().getStringExtra("telefonoAgenda");
        recibeName = getIntent().getStringExtra("nameViajeAgenda");
        key = getIntent().getStringExtra("llaveAgenda");
        recibeUnit = getIntent().getStringExtra("unidadAgenda");
        recibeParam = getIntent().getStringExtra("paramAgenda");
        recibeVal = getIntent().getStringExtra("valorAgenda");
        recibePhoneLogin = getIntent().getStringExtra("telAgenda");
        pantalla = "secundarias";
        //nombreUnidad = getIntent().getStringExtra("nombreUnidad");

        numeroViaje = findViewById(R.id.lbl_viaje);
        phoneAgenda = findViewById(R.id.lbl_telefono);
        lblNoViaje = findViewById(R.id.xml_nolblViaje);
        lblHourSalida = findViewById(R.id.xml_lblSalidaHour);
        lblHourArrive = findViewById(R.id.xml_lblLlegada);
        lblOrigen = findViewById(R.id.xml_lblOrigen);
        lblDestino = findViewById(R.id.xml_lblDestino);
        lblnumero = findViewById(R.id.numEmergencia);
        lblInstrucciones =  findViewById(R.id.indSeguridad);
        btnImgRoute = findViewById(R.id.xml_btnImgRoute);
        btnImgSos = findViewById(R.id.xml_btnImgSos);
        llaveA = findViewById(R.id.lbl_llave);
        unidadA = findViewById(R.id.lbl_unit);
        paramA = findViewById(R.id.lbl_param);
        valorA = findViewById(R.id.lbl_valida);
        back = findViewById(R.id.back);
        nomUnidad = findViewById(R.id.xml_nolblunidad);
        lbltelLogin = findViewById(R.id.lbl_telefonoLogin);

        numeroViaje.setText(recibeName);
        phoneAgenda.setText(recibePhone);
        llaveA.setText(key);
        unidadA.setText(recibeUnit);
        paramA.setText(recibeParam);
        valorA.setText(recibeVal);
        //nomUnidad.setText(nombreUnidad);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        String password = "C0ntr0lCCTSky19";
        MessageSender.initWithHost(host,port,key,password);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            new getViajeInfo().execute(recibePhone, recibeName);
        } else {
            Toast.makeText(getApplicationContext(),"!No tienes conexión a internet, revisa la misma y vuelve a ingresar¡",Toast.LENGTH_LONG).show();
        }

        btnImgRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ViajeActivity.this, "Mostrando la ruta", Toast.LENGTH_LONG).show();
                displayRoute(str);
            }
        });

        btnImgSos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    accederPermisoLocation();
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(ViajeActivity.this,"Mensaje guardado, debido a perdida de conexión de Internet.",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    private class getViajeInfo extends AsyncTask<String, Void, String> {
        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            counter = 1;
            progressDialog = new ProgressDialog(ViajeActivity.this);
            progressDialog.setMessage("Cargando información...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            System.out.println("Params: " + params);
            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("telefono", params[0]);
                paramObject.put("numero_viaje", params[1]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();
            //ejemplo en la linea 250 /viaje
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/get/agendaProg")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                if(response.code() == 404){
                    saveResponse =  response.body().string().toString();
                }else {
                    if(response.code() == 400){
                        saveResponse =  response.body().string().toString();
                    }else{
                        if(response.isSuccessful()){
                            saveResponse =  response.body().string().toString();
                        }else{
                            saveResponse = response.body().string().toString();
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        protected void onProgressUpdate(Integer... values) {
            //super.onProgressUpdate(values);
            progressDialog.setProgress((int)(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                if(result.isEmpty()){
                    progressDialog.cancel();
                    String msg = "Error en el Id de viaje, favor de verificar.";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                }else {
                    JSONObject jo = new JSONObject(result);
                    String success = jo.getString("success");

                    if (success != "false") {
                        progressDialog.cancel();
                        lblNoViaje.setText(jo.getString("id_viaje"));
                        lblOrigen.setText(jo.getString("origen"));
                        lblDestino.setText(jo.getString("destino"));
                        lblHourSalida.setText(jo.getString("hour_exist"));
                        lblHourArrive.setText(jo.getString("hour_arrive"));
                        lblInstrucciones.setText(jo.getString("info_seguridad"));
                        lblnumero.setText(jo.getString("telefonos_emergencia"));
                    } else {
                        progressDialog.cancel();
                        String msg = "Sin información del viaje";
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    }
                    String odm = lblNoViaje.getText().toString();
                    new getCoordOdm().execute(odm);
                }
            } catch (JSONException e) {
                progressDialog.cancel();
                Toast.makeText(getApplicationContext(),"Upss, ocurrio un error.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

        }
    }

    private void displayRoute(String coordenadas){
        try{
            Uri uri = Uri.parse("https://www.google.co.in/maps/dir/" + coordenadas);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            intent.setPackage("com.google.android.apps.maps");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }catch (ActivityNotFoundException e){
            Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.maps");
            Intent intentI = new Intent(Intent.ACTION_VIEW, uri);
            intentI.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intentI);
        }
    }

    private void accederPermisoLocation() {

        //si la API 23 a mas
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Habilitar permisos para la version de API 23 a mas

            int verificarPermisoLocationCoarse = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int verificarPermisoLocationFine = ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
            //Verificamos si el permiso no existe
            if (verificarPermisoLocationCoarse != PackageManager.PERMISSION_GRANTED && verificarPermisoLocationFine != PackageManager.PERMISSION_GRANTED) {
                //verifico si el usuario a rechazado el permiso anteriormente
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION) && shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Si a rechazado el permiso anteriormente muestro un mensaje

                    //mostrarExplicacion();
                } else {
                    //De lo contrario carga la ventana para autorizar el permiso
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                }

            } else {
                //Si el permiso fue activado llamo al metodo de ubicacion
                fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(android.location.Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {

                            linkLocation[0] = "http://maps.google.com/maps?q=loc:"+location.getLatitude()+","+location.getLongitude();

                            new registerEvent().execute(recibeUnit,String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()),"SOS enviado, atenta reacción, favor de marcar o actuar de manera veloz.");

                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                            Message message = new Message()
                                    .time(new Date().getTime())
                                    .location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                    .Sos()
                                    .batteryLevel((byte)batLevel)
                                    .text("Se ha presionado SOS en la ubicación: " + linkLocation[0] + " con nivel de bateria: " + batLevel + "%");

                            MessageSender.sendMessage(message,new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                        }else{

                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

                            Message message = new Message()
                                    .time(new Date().getTime())
                                    .Sos()
                                    .batteryLevel((byte)batLevel)
                                    .text("Se ha presionado SOS en la ubicación: " + linkLocation[0] + " con nivel de bateria: " + batLevel + "%");

                            MessageSender.sendMessage(message,new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
        } else {//Si la API es menor a 23 - llamo al metodo de ubicacion
            fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<android.location.Location>() {
                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {

                        linkLocation[0] = "http://maps.google.com/maps?q=loc:"+location.getLatitude()+","+location.getLongitude();

                        new registerEvent().execute(recibeUnit,String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()),"OPERADOR: SOS ENVIADO, ATENTA REACCIÓN, FAVOR DE MARCAR O ACTUAR DE MANERA VELOZ.");

                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = 0;

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        }

                        Message message = new Message()
                                .time(new Date().getTime())
                                .location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                .Sos()
                                .batteryLevel((byte)batLevel)
                                .text("Se ha presionado SOS en la ubicación: " + linkLocation[0] + " con nivel de bateria: " + batLevel + "%");

                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                    }
                    else{

                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = 0;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        }
                        //new ChatActivity.sendMessageTelegram().execute("Se ha presionado SOS en la ubicación: " + linkLocation[0]);
                        Message message = new Message()
                                .time(new Date().getTime())
                                .Sos()
                                .batteryLevel((byte)batLevel)
                                .text("Se ha presionado SOS en la ubicación: desconocida con nivel de bateria: " + batLevel + "%");

                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
    }

    private class getCoordOdm extends AsyncTask<String, Void, String> {

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ViajeActivity.this);
            progressDialog.setMessage("Cargando resumen del día ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            JSONObject paramObject = new JSONObject();
            try {
                System.out.println("parametros: " + params);
                System.out.println("cuenta: " + params[0]);
                paramObject.put("numero_viaje",  params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/coordenadasOdm")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        protected void onProgressUpdate(Integer... values) {
            //super.onProgressUpdate(values);
            progressDialog.setProgress((int)(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jo = new JSONObject(result);
                String success = jo.getString("success");

                if(success != "false"){
                    progressDialog.cancel();
                    values.clear();
                    for (int j = 0; j < jo.getJSONArray("inf").length(); j++) {

                        coordenadas = jo.getJSONArray("inf").getJSONArray(j).get(1).toString();
                        values.add(coordenadas);
                        System.out.println("coordenadas: " + coordenadas);
                    }

                    for (String s : values) {
                        sb.append(s);
                        sb.append("/");
                    }
                    str = sb.toString();
                    System.out.println(str);
                }else{
                    progressDialog.cancel();
                    String msg = jo.getString("message");
                    Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                progressDialog.cancel();
                Toast.makeText(getBaseContext(),"Error en .",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    public class registerEvent extends AsyncTask<String, Void, String>{

        String saveResponse = "";

        @Override
        protected String doInBackground(String... strings) {
            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("unit_id", strings[0]);
                paramObject.put("unit_x", strings[1]);
                paramObject.put("unit_y", strings[2]);
                paramObject.put("unit_txt", strings[3]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/add/event")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            //System.out.println(.*)
            return saveResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //System.out.println(.*)
            try {
                JSONObject jo = new JSONObject(result);
                String success = jo.getString("success");

                if(success != "false"){
                    Toast.makeText(getApplicationContext(), "Notificación registrada exitosamente", Toast.LENGTH_LONG).show();
                }else{
                    String msg = jo.getString("message");
                    Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                }


            } catch (JSONException e) {
                Toast.makeText(getBaseContext(),"Error en el servidor, para registro de evento.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    private int verificarSenal(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        return;
    }
}