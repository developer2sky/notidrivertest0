package us.skyguardian.driverns6;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A login screen that offers login via phone.
 */
public class LoginActivity extends AppCompatActivity {

    // UI references.
    // Botones de inicio de sesión.
    Button btn;
    TextInputEditText telefonoText, llaveText;
    TextInputLayout telefonoInputLayout, llaveInputLayout;
    ProgressDialog progressDialog;
    Integer counter = 1;
    String Phone, key;


    //private ProgressBar progressBar;
    private TextView lblloading;
    private static final int LOCATION_COARSE = 100;
    private static final int LOCATION_FINE = 101;
    private static final int CAMERA = 103;
    private static final int WRITE_EXTERNAL_STORAGE_CODE = 104;
    private static final int READ_EXTERNAL_STORAGE_CODE = 105;
    private static final int CALLPHONE_CODE = 106;
    private static final int READ_PHONE_STATE = 107;

    Integer validaInicioSesion = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        telefonoInputLayout = (TextInputLayout) findViewById(R.id.telefonoLayout);
        llaveInputLayout = (TextInputLayout) findViewById(R.id.llaveLayout);
        telefonoText = (TextInputEditText) findViewById(R.id.telefono);
        llaveText = (TextInputEditText) findViewById(R.id.llave);
        btn = (Button) findViewById(R.id.entrarButton);
        final SharedPreferences prefs = getSharedPreferences("MisCredenciales", Context.MODE_PRIVATE);
        String userText = prefs.getString("telefonoText", "");
        String passText = prefs.getString("llaveText", "");

        telefonoText.setText(userText);
        llaveText.setText(passText);

        accederPermisoLocation();

        btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("telefonoText", telefonoText.getText().toString());
                editor.putString("llaveText", llaveText.getText().toString());
                editor.commit();
                if (telefonoText.getText().toString().trim().equalsIgnoreCase("")){
                    telefonoInputLayout.clearAnimation();
                    telefonoInputLayout.setError("                !Este campo no puede estar vacío¡");
                }else if(llaveText.getText().toString().trim().equalsIgnoreCase("")){
                    llaveInputLayout.setError("               !Este campo no puede estar vacío¡");
                }else{
                    String auxphone = "+52" + telefonoText.getText().toString().trim();
                    Phone = telefonoText.getText().toString().trim();
                    String auxkey = llaveText.getText().toString().trim();
                    key = llaveText.getText().toString().trim();

                    ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                    if (networkInfo != null && networkInfo.isConnected()) {
                        new getTripsForPhoneTask().execute(auxphone, auxkey);
                    } else {
                        Toast.makeText(getApplicationContext(),"!No tienes conexión a internet, revisa la misma y vuelve a ingresar¡",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    private class getTripsForPhoneTask extends AsyncTask<String, Void, String> {

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            counter = 1;
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Accediendo al sistema...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            System.out.println("Params: " + params);
            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("telefono", params[0]);
                paramObject.put("key", params[1]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/get/getSession")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                if(response.code() == 404){
                    saveResponse =  response.body().string().toString();
                }else {
                    if(response.code() == 400){
                        saveResponse =  response.body().string().toString();
                    }else{
                        if(response.isSuccessful()){
                            saveResponse =  response.body().string().toString();
                        }else{
                            saveResponse = response.body().string().toString();
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        protected void onProgressUpdate(Integer... values) {
            progressDialog.setProgress((int)(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                if(result.isEmpty()){
                    progressDialog.cancel();
                    String msg = "Error en número celular o llave móvil, verifique e intentelo de nuevo.";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                }else {
                    JSONObject jo = new JSONObject(result);
                    String success = jo.getString("success");

                    if (success != "false") {
                        Toast.makeText(getApplicationContext(),"¡Bienvenido a Notidriver!",Toast.LENGTH_LONG).show();
                        progressDialog.cancel();
                        Intent intent = new Intent (LoginActivity.this, HomeActivity.class);
                        intent.putExtra("param", result);
                        intent.putExtra("phone", Phone);
                        intent.putExtra("llave", key);
                        startActivity(intent);
                    } else {
                        progressDialog.cancel();
                        String msg = jo.getString("message");
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                progressDialog.cancel();
                Toast.makeText(getApplicationContext(),"Upss, ocurrio un error intentalo de nuevo.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

        }
    }

    private void accederPermisoLocation() {

        //si la API 23 a mas
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Habilitar permisos para la version de API 23 a mas

            int verificarPermisoLocationCoarse = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int verificarPermisoLocationFine = ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
            //Verificamos si el permiso no existe
            if (verificarPermisoLocationCoarse != PackageManager.PERMISSION_GRANTED && verificarPermisoLocationFine != PackageManager.PERMISSION_GRANTED) {
                //verifico si el usuario a rechazado el permiso anteriormente
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION) && shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Si a rechazado el permiso anteriormente muestro un mensaje
                    mostrarExplicacion();
                } else {
                    //De lo contrario carga la ventana para autorizar el permiso
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                }
            } else {
            }
        }
        accedePermisoPhoto();
    }

    private void accedePermisoPhoto(){
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA))) {
                mostrarExplicacionPhoto();

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA);
            }
        }else{
            //El permiso ya fue activado
        }
        accedePermisoWriteStorage();
    }

    private void accedePermisoWriteStorage(){
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE))) {
                mostrarExplicacionWriteStorage();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_CODE);
            }
        }else{
            //El permiso ya fue activado
        }
        accedePermisoReadStorage();
    }

    private void accedePermisoReadStorage(){
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE))) {
                mostrarExplicacionReadStorage();

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE_CODE);
            }
        }else{
            //El permiso ya fue activado
        }
        accedePermisoCallphone();
    }

    private void accedePermisoCallphone(){
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE))) {
                mostrarExplicacionCallPhone();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, CALLPHONE_CODE);
            }
        }else{
            //El permiso ya fue activado
        }
        accedePermisoStatePhone();
    }

    private void accedePermisoStatePhone(){
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE))) {
                mostrarExplicacionStatePhone();

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE);
            }
        }else{
            //El permiso ya fue activado
        }
        //accedePermisoWriteStorage();
    }

    private void mostrarExplicacion() {
        new android.app.AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para acceder a la ultima y actual ubicación del dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    private void mostrarExplicacionPhoto() {
        new android.app.AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para acceder a la camara del dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.CAMERA}, CAMERA);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    private void mostrarExplicacionWriteStorage() {
        new android.app.AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para acceder al almacenamiento del dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_CODE);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    private void mostrarExplicacionReadStorage() {
        new android.app.AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para lectura al almacenamiento del dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE_CODE);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    private void mostrarExplicacionCallPhone() {
        new android.app.AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para realizar llamadas en este dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.CALL_PHONE}, CALLPHONE_CODE);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    private void mostrarExplicacionStatePhone() {
        new android.app.AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para realizar configuración en este dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    public void mensajeAccionCancelada() {
        Toast.makeText(getApplicationContext(), "Haz rechazado la petición, puede suceder que la app no trabaje de manera adecuada.", Toast.LENGTH_SHORT).show();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_COARSE:
                //Si el permiso a sido concedido abrimos la agenda de contactos
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accederPermisoLocation();
                } else {
                }
                break;
            case LOCATION_FINE:
                //Si el permiso a sido concedido abrimos la agenda de contactos
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accederPermisoLocation();
                } else {
                }
                break;
            case CAMERA:
                //System.out.println(.*)
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accedePermisoPhoto();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;

            case WRITE_EXTERNAL_STORAGE_CODE:
                //System.out.println(.*)
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accedePermisoWriteStorage();
                } else {

                }
                break;

            case READ_EXTERNAL_STORAGE_CODE:
                //System.out.println(.*)
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accedePermisoReadStorage();
                } else {
                }
                break;

            case CALLPHONE_CODE:
                //System.out.println(.*)
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accedePermisoCallphone();

                } else {
                }
                break;

            case READ_PHONE_STATE:
                //System.out.println(.*)
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accedePermisoStatePhone();

                } else {
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
    }
}

